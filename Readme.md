# VOYO

## Techno : 

- ReactJS 
- Typescript 
- TailwindCSS

- NodeJS 
- Express

- MySQL 

## Les étapes à suivre pour lancer le projet 

### Frontend

`cd client`

`npm install`

`npm start`

URL : http://localhost:3000/

### Backend

`cd serveur`

`npm install`

`npm start`

URL : http://localhost:8800/api 
un message {"message":"Server is started"} doit apparaitre. 