import Axios from 'axios'

const SERVER_APP_DOMAIN = process.env.REACT_APP_SERVER_APP_DOMAIN

export const createElementVisite = async (
  token: string,
  nom: string,
  commentaire: string,
  visiteId: number,
) => {
  try {
    const response = await Axios.post(
      `${SERVER_APP_DOMAIN}api/element-visite`,
      {
        nom,
        commentaire,
        visiteId,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    )

    return response.data
  } catch (error) {
    console.error('Erreur lors de la requête :', error)
    throw error
  }
}

export const getElementVisiteById = async (token: string, elementVisiteId: number) => {
  try {
    const response = await Axios.get(`${SERVER_APP_DOMAIN}api/element-visite/${elementVisiteId}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })

    return response.data
  } catch (error) {
    console.error('Erreur lors de la requête :', error)
    throw error
  }
}

export const deleteElementVisite = async (token: string, elementVisiteId: number) => {
  try {
    const response = await Axios.delete(
      `${SERVER_APP_DOMAIN}api/element-visite/${elementVisiteId}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    )

    return response.data
  } catch (error) {
    console.error('Erreur lors de la requête :', error)
    throw error
  }
}

export const getAllElementVisites = async (token: string, visiteId: number) => {
  try {
    const response = await Axios.get(`${SERVER_APP_DOMAIN}api/element-visite`, {
      params: { visiteId },
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    return response.data
  } catch (error) {
    console.error('Erreur lors de la requête :', error)
    throw error
  }
}

export const updateElementVisite = async (
  token: string,
  elementVisiteId: number,
  nom: string,
  commentaire: string,
  visiteId: number,
) => {
  try {
    const response = await Axios.put(
      `${SERVER_APP_DOMAIN}api/element-visite/${elementVisiteId}?visiteId=${visiteId}`,
      {
        nom,
        commentaire,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    )

    return response.data
  } catch (error) {
    console.error('Erreur lors de la requête :', error)
    throw error
  }
}
