import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

const AuthRedirect = () => {
  const navigate = useNavigate();

  useEffect(() => {
    const urlParams = new URLSearchParams(window.location.search);
    const token = urlParams.get('token');

    if (token) {
      localStorage.setItem('token', token);
      navigate('/')
    }
  }, [navigate]);

  return (
    <div>
      <p>Redirection en cours...</p>
    </div>
  );
};

export default AuthRedirect;
