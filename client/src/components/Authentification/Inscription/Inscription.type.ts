export type InscriptionData = {
  email: string;
  firstname: string;
  lastname: string;
  password: string;
  birthdate: string;
  city: string;
};