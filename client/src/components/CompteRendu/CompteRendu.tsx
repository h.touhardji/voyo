import React, { useEffect, useState } from 'react'
import withAuth from '../Security/withAuth'
import Footer from '../Footer/Footer'
import Menu from '../Menu/Menu'
import { useParams } from 'react-router-dom'

import {
  createElementVisite,
  deleteElementVisite,
  getAllElementVisites,
  updateElementVisite,
} from '../../api/ElementVisite'

interface CompteRenduProps {
  authState: {
    email: string
    firstname: string
    lastname: string
    phoneNumber: string
    createdAt: Date
    updatedAt: Date
    userImg: string
    birthdate: string
    city: string
    isVisiteur: boolean
  }
  authProps: {
    token: string
    isLoggedIn: boolean
    setIsLoggedIn: React.Dispatch<React.SetStateAction<boolean>>
  }
}

interface ElementVisite {
  id: number
  nom: string
  commentaire: string
}

const CompteRendu: React.FC<CompteRenduProps> = ({ authState, authProps }) => {
  const [elementsAVisiter, setElementsAVisiter] = useState<ElementVisite[]>([])

  const [nouvelElement, setNouvelElement] = useState('')
  const [elementEnEdition, setElementEnEdition] = useState<ElementVisite | null>(null)
  const { id } = useParams()
  const visiteId = Number(id)
  useEffect(() => {
    const fetchElementVisites = async () => {
      try {
        const result = await getAllElementVisites(authProps.token, visiteId)
        console.log(result)

        setElementsAVisiter(result.elementVisites)
      } catch (error) {
        console.error(error)
      }
    }

    if (authProps.token && visiteId) {
      fetchElementVisites()
    }
  }, [authProps.token, visiteId])

  const handleAjouterElement = async () => {
    if (nouvelElement.trim() !== '') {
      try {
        const result = await createElementVisite(authProps.token, nouvelElement, '', visiteId)
        setElementsAVisiter([...elementsAVisiter, result.elementVisite])
        setNouvelElement('')
      } catch (error) {
        console.error('Erreur lors de ajout un nouvel élément à visiter :', error)
      }
    }
  }

  const handleModifierElement = (element: ElementVisite) => {
    setElementEnEdition(element)
    setNouvelElement(element.nom)
  }

  const handleAjoutCommentaire = (element: ElementVisite, nouveauCommentaire: string) => {
    const nouveauxElements = [...elementsAVisiter]
    const indexElement = nouveauxElements.findIndex((el) => el.id === element.id)

    if (indexElement !== -1) {
      nouveauxElements[indexElement] = {
        ...nouveauxElements[indexElement],
        commentaire: nouveauCommentaire,
      }

      setElementsAVisiter(nouveauxElements)
    }
  }

  const handleEnregistrerModification = async (id: number) => {
    // Vérifier si l'élément est en édition
    if (elementEnEdition) {
      try {
        await updateElementVisite(
          authProps.token,
          elementEnEdition.id,
          nouvelElement,
          elementEnEdition.commentaire,
          visiteId,
        )
        const updatedElements = elementsAVisiter.map((element) =>
          element.id === elementEnEdition.id ? { ...element, nom: nouvelElement } : element,
        )
        setElementsAVisiter(updatedElements)
        setNouvelElement('')
        setElementEnEdition(null)
      } catch (error) {
        console.error('Erreur lors de la mise à jour de l élément :', error)
      }
    }
  }

  const handleSupprimerElement = async (element: ElementVisite) => {
    try {
      await deleteElementVisite(authProps.token, element.id)
      const filteredElements = elementsAVisiter.filter((e) => e.id !== element.id)
      setElementsAVisiter(filteredElements)
    } catch (error) {
      console.error('Erreur lors de la suppression de l élément :', error)
    }
  }

  return (
    <div className='bg-gray-100 min-h-screen'>
      <Menu authState={authState} authProps={authProps} />
      <div className='container mx-auto py-32 '>
        <h1 className='text-3xl font-semibold mb-10 mt-10'>Éléments à visiter</h1>
        <ul className='list-disc'>
          {elementsAVisiter.map((element) => (
            <li
              key={element.id}
              className='flex items-center justify-between bg-white p-10 rounded-md shadow-md mb-4'
            >
              {element.id === elementEnEdition?.id ? (
                <div key={element.id} className='flex flex-col w-full space-y-4'>
                  <span>{element.nom}</span>
                  <textarea
                    className='p-8 h-60 border rounded-md bg-gray-100'
                    value={element.commentaire || ''}
                    onChange={(e) => handleAjoutCommentaire(element, e.target.value)}
                  />
                  <button
                    onClick={() => handleEnregistrerModification(element.id)}
                    className='bg-blue-500 text-white px-4 w-fit rounded-md bg-orange'
                  >
                    Enregistrer
                  </button>
                </div>
              ) : (
                <div className='flex flex items-center space-x-4 justify-between w-full'>
                  <span className='text-lg'>{element.nom}</span>
                  <div className='space-x-2 flex'>
                    <button
                      onClick={() => handleModifierElement(element)}
                      className='text-white bg-blue-500 hover:bg-blue-600'
                    >
                      Modifier
                    </button>
                    <button
                      onClick={() => handleSupprimerElement(element)}
                      className='text-white bg-red-500 hover:bg-red-600'
                    >
                      Supprimer
                    </button>
                  </div>
                </div>
              )}
            </li>
          ))}
        </ul>
        <div className='mt-4 flex'>
          <input
            type='text'
            placeholder='Nouvel élément à visiter'
            value={nouvelElement}
            onChange={(e) => setNouvelElement(e.target.value)}
            className='border p-2 rounded-md'
          />
          <button
            onClick={handleAjouterElement}
            className='bg-green-500 text-white px-4 py-2 rounded-md ml-2 hover:bg-green-600'
          >
            Ajouter
          </button>
        </div>
      </div>
      <Footer />
    </div>
  )
}

export default withAuth(CompteRendu)
