import './BoiteAuxLettres.css'
import { SetStateAction, useEffect, useState } from 'react'
import { getChatsByUser } from '../../../api/Chat'
import { timeAgo } from '../../../utils/Data.utils'
import ChatPage from '../Messagerie/ChatPage'
import etourneau from '../../../assets/images/image_oiseaux/etourneau.png'
import { io } from 'socket.io-client'

interface BoiteAuxLettresProps {
  token: string
  authState: {
    email: string
    firstname: string
    lastname: string
    phoneNumber: string
    createdAt: Date
    updatedAt: Date
    userImg: string
    birthdate: string
    city: string
    isVisiteur: boolean
  }
}

type ChatType = {
  createdAt: string
  email: string
  id: number
  visiteId: number
  otherUser: {
    firstname: string
    lastname: string
    city: string
    userImg: string
    email: string
  }
  status: number
  updatedAt: string
}

const BoiteAuxLettres: React.FC<BoiteAuxLettresProps> = ({
  token,
  authState,
}: BoiteAuxLettresProps) => {
  const [selectedBoiteAuxLettresItem, setSelectedBoiteAuxLettresItem] = useState(0)

  const [userChatList, setUserChatList] = useState<ChatType[]>([])
  const [chat, setChat] = useState<ChatType>()
  const [isUserChatsLoading, setIsUserChatsLoading] = useState(false)
  const [userChatsError, setUserChatsError] = useState(null)
  const [displayChat, setDisplayChat] = useState(false)

  const serverAppDomain = process.env.REACT_APP_WEBSOCKET_URL
  const socket = io(serverAppDomain as string, { transports: ['websocket'] })

  useEffect(() => {
    const getUserChats = async () => {
      if (token) {
        setIsUserChatsLoading(true)
        setUserChatsError(null)
        try {
          const response = await getChatsByUser(token)

          setIsUserChatsLoading(false)
          const userEmail = authState.email
          if (response.error) {
            setUserChatsError(response)
          } else {
            const filteredChats = response.message
              .filter((chat: any) => {
                return chat.User1Email === userEmail || chat.User2Email === userEmail
              })
              .map((chat: any) => {
                const otherUser = chat.User1Email === userEmail ? chat.User2 : chat.User1
                return {
                  id: chat.id,
                  status: chat.status,
                  createdAt: chat.createdAt,
                  updatedAt: chat.updatedAt,
                  email: userEmail,
                  visiteId: chat.visiteId,
                  otherUser: {
                    firstname: otherUser.firstname,
                    lastname: otherUser.lastname,
                    city: otherUser.city,
                    userImg: otherUser.userImg,
                    email: otherUser.email,
                  },
                }
              })
            console.log(filteredChats)

            setUserChatList(filteredChats)
          }
        } catch (error: any) {
          setIsUserChatsLoading(false)
          setUserChatsError(error)
        }
      }
    }

    getUserChats()
  }, [token])

  useEffect(() => {
    socket.emit('newUser', authState.email)
  })

  const menuItems = [
    { label: 'Demandes en attente' },
    { label: 'Visites à venir' },
    { label: 'Visites passées' },
    { label: 'Demandes archivées' },
  ]

  const handleMenuItemClick = (index: SetStateAction<number>) => {
    setSelectedBoiteAuxLettresItem(index)
  }

  const renderContent = (chatList: ChatType[]) => {
    const filteredChat = chatList.filter(
      (chat: ChatType) => chat.status === selectedBoiteAuxLettresItem + 1,
    )

    return (
      <div className='list-message'>
        {filteredChat.map((chat: any) => (
          <div
            key={chat.id}
            className='message'
            onClick={() => {
              setDisplayChat(!displayChat)
              setChat(chat)
            }}
          >
            <div className='message-content'>
              <img
                src={chat?.otherUser?.userImg ? chat?.otherUser?.userImg : etourneau}
                alt='man'
              />
              <div>
                <div className='message-content-user-info'>
                  <h4>{chat?.otherUser?.lastname}</h4>
                  <h4>{chat?.otherUser?.firstname}</h4>
                </div>
                <p>Modifié il y a {timeAgo(chat?.updatedAt)}</p>
              </div>
              <p></p>
            </div>
          </div>
        ))}
      </div>
    )
  }

  return (
    <div className='flex justify-between'>
      <div className='flex flex-col w-full'>
        <ul
          className={`flex items-start rounded-lg bg-orange-100 mb-5 ${
            displayChat ? 'w-full ' : 'w-fit'
          }`}
        >
          {menuItems.map((item, index) => (
            <li
              key={index}
              onClick={() => handleMenuItemClick(index)}
              className={`boiteAuxLettres-menu-item ${
                selectedBoiteAuxLettresItem === index ? 'boiteAuxLettres-menu-item-active' : ''
              }`}
            >
              {item.label}
            </li>
          ))}
        </ul>
        <select
          className='hidden md:block'
          value={selectedBoiteAuxLettresItem}
          onChange={(e) => handleMenuItemClick(Number(e.target.value))}
        >
          {menuItems.map((item, index) => (
            <option
              key={index}
              value={index}
              className={`select-boiteAuxLettres-menu-item ${
                selectedBoiteAuxLettresItem === index ? 'boiteAuxLettres-menu-item-active' : ''
              }`}
            >
              {item.label}
            </option>
          ))}
        </select>
        <div className={displayChat && chat ? 'hidden' : 'content-container'}>
          {renderContent(userChatList)}
        </div>
        {displayChat && (
          <ChatPage
            setDisplayChat={setDisplayChat}
            chat={chat}
            authState={authState}
            token={token}
          />
        )}
      </div>
      <div
        className={`flex flex-col items-start ${
          displayChat ? 'ml-4 rounded-2xl space-y-11 ' : 'hidden'
        }`}
      >
        <div className='bg-orange-700 text-white p-4 rounded-2xl w-80'>
          <p className='font-bold mb-2 text-xl'>Demande de visite</p>
          <div className='mb-2'>
            <p>Type de logement :</p>
          </div>
          <div className='mb-2'>
            <p>Date / heure :</p>
          </div>
          <div className='mb-2'>
            <p>Propriétaire ou agence :</p>
          </div>
          <input
            type='text'
            className='w-full p-2 mb-2 border-gray-300 rounded'
            placeholder='Votre prix...'
          />
          <p className='text-xs italic text-gray-300'>
            Réserver et payer par Voyo est requis par les conditions d&apos;utilisation.
            N&apos;acceptez jamais d&apos;espèces.
          </p>
        </div>
        <div className='flex items-center w-full'>
          <img
            src={chat?.otherUser.userImg}
            alt=''
            className='w-10 h-10 object-cover rounded-full shadow mr-2'
          />
          <div>
            <p className='text-base font-medium'>{chat?.otherUser.firstname}</p>
            <p className='text-base text-gray-500'>{chat?.otherUser.lastname}</p>
          </div>
        </div>
        <div className='flex items-center justify-center w-full'>
          <a href={`/compte-rendu/${chat?.visiteId}`} className='btn btn-orange bg-orange w-fit'>
            Commencer le compte rendu
          </a>
        </div>
      </div>
    </div>
  )
}

export default BoiteAuxLettres
