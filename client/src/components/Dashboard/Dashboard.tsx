import logo from '../../assets/images/logo_final/logo.png'
import etourneau from '../../assets/images/image_oiseaux/etourneau.png'
import './Dashboard.css'
import { SetStateAction, useState } from 'react'
import BoiteAuxLettres from './BoiteAuxLettres/BoiteAuxLettres'
import { Clear, Menu } from '@mui/icons-material'
import withAuth from '../Security/withAuth'
import Profil from './Profil/Profil'
import Parametres from './Parametres/Parametres'
import Disponibilite from './Disponibilite/Disponibilite'

interface DashboardProps {
  authState: {
    email: string
    firstname: string
    lastname: string
    phoneNumber: string
    createdAt: Date
    updatedAt: Date
    userImg: string
    birthdate: string
    city: string
    isVisiteur: boolean
  }
  authProps: {
    token: string
    isLoggedIn: boolean
    setIsLoggedIn: React.Dispatch<React.SetStateAction<boolean>>
  }
}

const Dashboard: React.FC<DashboardProps> = ({ authState, authProps }: DashboardProps) => {
  const { token, setIsLoggedIn } = authProps
  const [selectedMenuItem, setSelectedMenuItem] = useState('profil')
  const [isMenuOpen, setIsMenuOpen] = useState(false)

  const toggleMenu = () => {
    setIsMenuOpen(!isMenuOpen)
  }

  // Fonction pour mettre à jour l'élément sélectionné
  const handleMenuItemClick = (menuItem: SetStateAction<string>) => {
    setSelectedMenuItem(menuItem)
  }

  return (
    <div className='dashboard'>
      <div className={`dashboard-menu ${isMenuOpen ? 'show-menu' : ''}`}>
        <div>
          <a href='/'>
            <img src={logo} alt='logo' />
          </a>
          <Clear onClick={toggleMenu} />
        </div>
        <a href='/'>
          <img src={logo} alt='logo' />
        </a>

        <ul className='dashboard-menu-content'>
          <li
            onClick={() => {
              handleMenuItemClick('profil')
              toggleMenu()
            }}
          >
            <svg
              xmlns='http://www.w3.org/2000/svg'
              width='20'
              height='20'
              viewBox='0 0 20 20'
              fill='none'
            >
              <path
                d='M14.9849 15.6039C14.4026 14.833 13.6493 14.2078 12.7842 13.7776C11.9192 13.3473 10.9661 13.1239 9.99993 13.1248C9.0338 13.1239 8.08067 13.3473 7.21561 13.7776C6.35056 14.2078 5.59721 14.833 5.01493 15.6039M14.9849 15.6039C16.1212 14.5932 16.9225 13.261 17.2842 11.784C17.646 10.3069 17.5501 8.75483 17.0095 7.33349C16.4688 5.91215 15.5089 4.68873 14.2569 3.8255C13.005 2.96227 11.5202 2.5 9.99951 2.5C8.47881 2.5 6.99404 2.96227 5.7421 3.8255C4.49016 4.68873 3.5302 5.91215 2.98954 7.33349C2.44888 8.75483 2.35306 10.3069 2.71478 11.784C3.0765 13.261 3.87868 14.5932 5.01493 15.6039M14.9849 15.6039C13.6133 16.8274 11.838 17.5024 9.99993 17.4998C8.16162 17.5026 6.38679 16.8276 5.01493 15.6039M12.4999 8.12475C12.4999 8.7878 12.2365 9.42368 11.7677 9.89252C11.2989 10.3614 10.663 10.6248 9.99993 10.6248C9.33689 10.6248 8.701 10.3614 8.23216 9.89252C7.76332 9.42368 7.49993 8.7878 7.49993 8.12475C7.49993 7.46171 7.76332 6.82583 8.23216 6.35699C8.701 5.88815 9.33689 5.62475 9.99993 5.62475C10.663 5.62475 11.2989 5.88815 11.7677 6.35699C12.2365 6.82583 12.4999 7.46171 12.4999 8.12475Z'
                stroke='#2B2B2B'
                strokeWidth='1.5'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
            </svg>
            Profil
          </li>
          {authState.isVisiteur && (
            <li
              onClick={() => {
                handleMenuItemClick('tableauDeBord')
                toggleMenu()
              }}
            >
              <svg
                xmlns='http://www.w3.org/2000/svg'
                width='20'
                height='20'
                viewBox='0 0 20 20'
                fill='none'
              >
                <path
                  d='M3.125 5C3.125 4.50272 3.32254 4.02581 3.67417 3.67417C4.02581 3.32254 4.50272 3.125 5 3.125H6.875C7.37228 3.125 7.84919 3.32254 8.20082 3.67417C8.55246 4.02581 8.75 4.50272 8.75 5V6.875C8.75 7.37228 8.55246 7.84919 8.20082 8.20082C7.84919 8.55246 7.37228 8.75 6.875 8.75H5C4.50272 8.75 4.02581 8.55246 3.67417 8.20082C3.32254 7.84919 3.125 7.37228 3.125 6.875V5ZM3.125 13.125C3.125 12.6277 3.32254 12.1508 3.67417 11.7992C4.02581 11.4475 4.50272 11.25 5 11.25H6.875C7.37228 11.25 7.84919 11.4475 8.20082 11.7992C8.55246 12.1508 8.75 12.6277 8.75 13.125V15C8.75 15.4973 8.55246 15.9742 8.20082 16.3258C7.84919 16.6775 7.37228 16.875 6.875 16.875H5C4.50272 16.875 4.02581 16.6775 3.67417 16.3258C3.32254 15.9742 3.125 15.4973 3.125 15V13.125ZM11.25 5C11.25 4.50272 11.4475 4.02581 11.7992 3.67417C12.1508 3.32254 12.6277 3.125 13.125 3.125H15C15.4973 3.125 15.9742 3.32254 16.3258 3.67417C16.6775 4.02581 16.875 4.50272 16.875 5V6.875C16.875 7.37228 16.6775 7.84919 16.3258 8.20082C15.9742 8.55246 15.4973 8.75 15 8.75H13.125C12.6277 8.75 12.1508 8.55246 11.7992 8.20082C11.4475 7.84919 11.25 7.37228 11.25 6.875V5ZM11.25 13.125C11.25 12.6277 11.4475 12.1508 11.7992 11.7992C12.1508 11.4475 12.6277 11.25 13.125 11.25H15C15.4973 11.25 15.9742 11.4475 16.3258 11.7992C16.6775 12.1508 16.875 12.6277 16.875 13.125V15C16.875 15.4973 16.6775 15.9742 16.3258 16.3258C15.9742 16.6775 15.4973 16.875 15 16.875H13.125C12.6277 16.875 12.1508 16.6775 11.7992 16.3258C11.4475 15.9742 11.25 15.4973 11.25 15V13.125Z'
                  stroke='black'
                  strokeWidth='1.5'
                  strokeLinecap='round'
                  strokeLinejoin='round'
                />
              </svg>
              Tableau de bord
            </li>
          )}
          <li
            onClick={() => {
              handleMenuItemClick('boiteAuxLettres')
              toggleMenu()
            }}
          >
            <svg
              xmlns='http://www.w3.org/2000/svg'
              width='20'
              height='20'
              viewBox='0 0 20 20'
              fill='none'
            >
              <path
                d='M18.125 7.49981V8.25481C18.125 8.59335 18.0334 8.92557 17.8598 9.21622C17.6862 9.50687 17.4372 9.74511 17.1392 9.90565L11.7408 12.8123M1.875 7.49981V8.25481C1.87499 8.59335 1.96663 8.92557 2.1402 9.21622C2.31377 9.50687 2.56279 9.74511 2.86083 9.90565L8.25917 12.8123M8.25917 12.8123L9.11083 12.354C9.38409 12.2068 9.68962 12.1297 10 12.1297C10.3104 12.1297 10.6159 12.2068 10.8892 12.354L11.7417 12.8123L15.625 14.904M8.25917 12.8123L4.375 14.904M18.125 16.2498C18.125 16.7471 17.9275 17.224 17.5758 17.5756C17.2242 17.9273 16.7473 18.1248 16.25 18.1248H3.75C3.25272 18.1248 2.77581 17.9273 2.42417 17.5756C2.07254 17.224 1.875 16.7471 1.875 16.2498V7.36981C1.87514 7.03142 1.96685 6.69937 2.14041 6.40888C2.31397 6.11839 2.56291 5.88029 2.86083 5.71981L9.11083 2.35315C9.38409 2.20596 9.68962 2.12891 10 2.12891C10.3104 2.12891 10.6159 2.20596 10.8892 2.35315L17.1392 5.71981C17.4371 5.88029 17.686 6.11839 17.8596 6.40888C18.0332 6.69937 18.1249 7.03142 18.125 7.36981V16.2498Z'
                stroke='#2B2B2B'
                strokeWidth='1.5'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
            </svg>
            Boites aux lettres
          </li>
          <li
            onClick={() => {
              handleMenuItemClick('parametres')
              toggleMenu()
            }}
          >
            <svg
              xmlns='http://www.w3.org/2000/svg'
              width='20'
              height='20'
              viewBox='0 0 20 20'
              fill='none'
            >
              <path
                d='M7.99509 3.28333C8.07009 2.83167 8.46176 2.5 8.92009 2.5H11.0809C11.5393 2.5 11.9309 2.83167 12.0059 3.28333L12.1834 4.35083C12.2359 4.6625 12.4443 4.9225 12.7209 5.07583C12.7826 5.10917 12.8434 5.145 12.9043 5.18167C13.1743 5.345 13.5043 5.39583 13.8001 5.285L14.8143 4.905C15.0221 4.82684 15.2509 4.82498 15.46 4.89976C15.6691 4.97454 15.8448 5.1211 15.9559 5.31333L17.0359 7.18583C17.1468 7.37809 17.1859 7.60344 17.1462 7.8218C17.1065 8.04017 16.9907 8.23737 16.8193 8.37833L15.9834 9.0675C15.7393 9.2675 15.6184 9.57833 15.6243 9.89417C15.6256 9.96499 15.6256 10.0358 15.6243 10.1067C15.6184 10.4217 15.7393 10.7317 15.9826 10.9317L16.8201 11.6217C17.1734 11.9133 17.2651 12.4167 17.0368 12.8133L15.9551 14.6858C15.8441 14.878 15.6686 15.0246 15.4597 15.0996C15.2508 15.1745 15.0221 15.1729 14.8143 15.095L13.8001 14.715C13.5043 14.6042 13.1751 14.655 12.9034 14.8183C12.843 14.8551 12.7819 14.8906 12.7201 14.925C12.4443 15.0775 12.2359 15.3375 12.1834 15.6492L12.0059 16.7158C11.9309 17.1683 11.5393 17.5 11.0809 17.5H8.91926C8.46092 17.5 8.06926 17.1683 7.99426 16.7167L7.81676 15.6492C7.76509 15.3375 7.55676 15.0775 7.28009 14.9242C7.2183 14.8901 7.15717 14.8548 7.09676 14.8183C6.82592 14.655 6.49676 14.6042 6.20009 14.715L5.18592 15.095C4.9782 15.1729 4.74956 15.1747 4.54067 15.0999C4.33178 15.0251 4.15618 14.8787 4.04509 14.6867L2.96426 12.8142C2.85338 12.6219 2.8143 12.3966 2.85397 12.1782C2.89364 11.9598 3.0095 11.7626 3.18092 11.6217L4.01759 10.9325C4.26092 10.7325 4.38176 10.4217 4.37592 10.1058C4.37462 10.035 4.37462 9.96416 4.37592 9.89333C4.38176 9.57833 4.26092 9.26833 4.01759 9.06833L3.18092 8.37833C3.00971 8.23741 2.89399 8.04036 2.85432 7.82219C2.81465 7.60401 2.8536 7.37884 2.96426 7.18667L4.04509 5.31417C4.15608 5.12178 4.33177 4.97505 4.54085 4.90011C4.74993 4.82518 4.97883 4.82691 5.18676 4.905L6.20009 5.285C6.49676 5.39583 6.82592 5.345 7.09676 5.18167C7.15676 5.145 7.21842 5.10917 7.28009 5.075C7.55676 4.9225 7.76509 4.6625 7.81676 4.35083L7.99509 3.28333Z'
                stroke='#2B2B2B'
                strokeWidth='1.5'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M12.5 10C12.5 10.663 12.2366 11.2989 11.7678 11.7678C11.2989 12.2366 10.663 12.5 10 12.5C9.33696 12.5 8.70107 12.2366 8.23223 11.7678C7.76339 11.2989 7.5 10.663 7.5 10C7.5 9.33696 7.76339 8.70107 8.23223 8.23223C8.70107 7.76339 9.33696 7.5 10 7.5C10.663 7.5 11.2989 7.76339 11.7678 8.23223C12.2366 8.70107 12.5 9.33696 12.5 10Z'
                stroke='#2B2B2B'
                strokeWidth='1.5'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
            </svg>
            Paramètres
          </li>
        </ul>
        <div
          className='logout'
          onClick={() => {
            localStorage.removeItem('token')
            setIsLoggedIn(false)
            window.location.reload()
          }}
        >
          <img src={authState.userImg ? authState.userImg : etourneau} alt='profile' />
          <div className='logout-profil'>
            <p className='name'>
              {authState.firstname} {authState.lastname}
            </p>
            <p className='email'>{authState.email}</p>
          </div>
          <svg
            xmlns='http://www.w3.org/2000/svg'
            width='20'
            height='20'
            viewBox='0 0 20 20'
            fill='none'
          >
            <path
              d='M13.3333 14.1667L17.5 10M17.5 10L13.3333 5.83333M17.5 10H7.5M7.5 2.5H6.5C5.09987 2.5 4.3998 2.5 3.86502 2.77248C3.39462 3.01217 3.01217 3.39462 2.77248 3.86502C2.5 4.3998 2.5 5.09987 2.5 6.5V13.5C2.5 14.9001 2.5 15.6002 2.77248 16.135C3.01217 16.6054 3.39462 16.9878 3.86502 17.2275C4.3998 17.5 5.09987 17.5 6.5 17.5H7.5'
              stroke='#2B2B2B'
              strokeWidth='1.66667'
              strokeLinecap='round'
              strokeLinejoin='round'
            />
          </svg>
        </div>
      </div>
      <div className='dashboard-content'>
        <div className='dashboard-content-header-mobile'>
          <img src={logo} alt='logo' />
          <Menu onClick={toggleMenu} />
        </div>

        {selectedMenuItem === 'tableauDeBord' ? (
          <Disponibilite token={authProps.token} />
        ) : selectedMenuItem === 'profil' ? (
          authState.firstname ? (
            <Profil authState={authState} authProps={authProps} />
          ) : (
            <p>Loading...</p>
          )
        ) : selectedMenuItem === 'boiteAuxLettres' ? (
          <BoiteAuxLettres token={token} authState={authState} />
        ) : selectedMenuItem === 'parametres' ? (
          <Parametres authProps={authProps} authState={authState} />
        ) : (
          ''
        )}
      </div>
    </div>
  )
}

export default withAuth(Dashboard)
