import React, { useEffect, useState, useRef, SetStateAction } from 'react'
import ChatBody from './ChatBody'
import ChatFooter from './ChatFooter'
import './chat.css'
import { Close } from '@mui/icons-material'
import etourneau from '../../../assets/images/image_oiseaux/etourneau.png'
import { io } from 'socket.io-client'

type Chat = {
  createdAt: string
  email: string
  id: number
  otherUser: {
    firstname: string
    lastname: string
    city: string
    userImg: string
    email: string
  }
  status: number
  updatedAt: string
}

type Message = {
  chatId: number
  content: string
  createdAt: Date
  id: number
  updatedAt: Date
  userEmail: string
}

type UserOnline = {
  socketID: string
  userId: string
}

interface ChatPageProps {
  authState: {
    email: string
    firstname: string
    lastname: string
    phoneNumber: string
    createdAt: Date
    updatedAt: Date
    userImg: string
    birthdate: string
    city: string
    isVisiteur: boolean
  }
  setDisplayChat: React.Dispatch<SetStateAction<boolean>>
  chat: Chat | undefined
  token: string
}

const ChatPage = ({ setDisplayChat, chat, authState, token }: ChatPageProps) => {
  const [messages, setMessages] = useState<Message[]>([])

  const [typingStatus, setTypingStatus] = useState('')
  const lastMessageRef = useRef(null)
  const [isUsersOnline, setIsUsersOnline] = useState(false)

  const serverAppDomain = process.env.REACT_APP_WEBSOCKET_URL
  const socket = io(serverAppDomain as string, { transports: ['websocket'] })

  useEffect(() => {
    socket.on('messageResponse', (data: any) => {
      setMessages([...messages, data])
    })
  }, [socket, messages])

  useEffect(() => {
    socket.on('onlineUsersUpdate', (data: UserOnline[]) => {
      setIsUsersOnline(data.some((user) => user.userId === chat?.otherUser.email))
    })
  }, [isUsersOnline])

  return (
    <div className='chat'>
      <div className='message__header'>
        <div className='flex items-center justify-center gap-4'>
          <div className='message__header-img-profil'>
            {isUsersOnline && <div className='round-user-Login'></div>}
            <img src={chat?.otherUser.userImg ? chat?.otherUser.userImg : etourneau} alt='man' />
          </div>
          <div className='flex gap-3 w-fil'>
            <p className='text-base font-medium'>{chat?.otherUser.firstname}</p>
            <p className='text-base text-gray-500'>{chat?.otherUser.lastname}</p>
          </div>
        </div>

        <Close onClick={() => setDisplayChat(() => false)} />
      </div>
      <div>
        <ChatBody
          messages={messages}
          typingStatus={typingStatus}
          lastMessageRef={lastMessageRef}
          setMessages={setMessages}
          chatId={chat?.id}
          token={token}
        />
        <ChatFooter
          chatId={chat?.id}
          socket={socket}
          messages={messages}
          setMessages={setMessages}
          token={token}
          authState={authState}
        />
      </div>
    </div>
  )
}

export default ChatPage
