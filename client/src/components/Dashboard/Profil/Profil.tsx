import { useEffect, useState } from 'react'
import withAuth from '../../Security/withAuth'
import { updateAccount } from '../../../api/User'
import { updateProfileInfo } from '../../../api/Profile'
import swal from 'sweetalert'
import etourneau from '../../../assets/images/image_oiseaux/etourneau.png'
import { getProfileVisiteur, updateProfileVisiteur } from '../../../api/ProfilVisiteur'

interface DashboardProfilProps {
  authState: {
    email: string
    firstname: string
    lastname: string
    phoneNumber: string
    createdAt: Date
    updatedAt: Date
    userImg: string
    birthdate: string
    city: string
    isVisiteur: boolean
  }
  authProps: {
    token: string
    isLoggedIn: boolean
    setIsLoggedIn: React.Dispatch<React.SetStateAction<boolean>>
  }
}

const Profil: React.FC<DashboardProfilProps> = ({ authState, authProps }: DashboardProfilProps) => {
  const { token } = authProps
  const [prenom, setPrenom] = useState(authState.firstname)
  const [nom, setNom] = useState(authState.lastname)
  const [city, setCity] = useState(authState.city)
  const [telephone, setTelephone] = useState(authState.phoneNumber)
  const [dateNaissance, setDateNaissance] = useState(authState.birthdate)
  const [imgSrc, setImgSrc] = useState(authState.userImg)
  const [isAvailable, setIsAvailable] = useState(true)

  const handleToggle = async () => {
    setIsAvailable((prev) => !prev)
    const data = {
      disponibleImmediatement: !isAvailable,
    }
    await updateProfileVisiteur(token, data)
  }

  const handleImageClick = () => {
    document?.getElementById('fileInput')?.click()
  }

  const handleFileChange = async (event: any) => {
    const file = event.target.files?.[0]
    const formData = new FormData()
    formData.append('userImg', file)
    try {
      const response = await updateProfileInfo(token, formData)

      if (file && response.success === true) {
        const reader = new FileReader()
        reader.onload = () => {
          const imageUrl = reader.result as string
          setImgSrc(imageUrl)
        }
        reader.readAsDataURL(file)
      }
    } catch (error: any) {
      console.log(error)
    }
  }

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await getProfileVisiteur(token)
        setIsAvailable(response.data.profileVisiteur.disponibleImmediatement)
      } catch (error) {
        console.error(error)
      }
    }

    fetchData()
  }, [token])

  return (
    <div>
      <div className='dashboard-content-text'>
        <h1>Profil</h1>
      </div>

      <div className='flex relative w-fit mb-10'>
        <img
          className='w-24 h-24 rounded-full shadow-md object-cover cover'
          src={imgSrc ? imgSrc : etourneau}
          alt='profile'
        />

        <div
          onClick={() => handleImageClick()}
          className='w-8 h-8 flex items-center justify-center rounded-full shadow-md object-cover cover bg-red-500 absolute bottom-0 right-0 cursor-pointer'
        >
          <input
            type='file'
            id='fileInput'
            style={{ display: 'none' }}
            onChange={handleFileChange}
          />
          <svg
            xmlns='http://www.w3.org/2000/svg'
            width='20'
            height='20'
            viewBox='0 0 20 20'
            fill='none'
          >
            <path
              d='M14.0514 3.73889L15.4576 2.33265C16.0678 1.72245 17.0572 1.72245 17.6674 2.33265C18.2775 2.94284 18.2775 3.93216 17.6674 4.54235L5.69349 16.5162C5.25292 16.9568 4.70953 17.2806 4.11241 17.4585L1.875 18.125L2.54148 15.8876C2.71936 15.2905 3.04321 14.7471 3.48377 14.3065L14.0514 3.73889ZM14.0514 3.73889L16.25 5.93749'
              stroke='white'
              strokeWidth='1.5'
              strokeLinecap='round'
              strokeLinejoin='round'
            />
          </svg>
        </div>
      </div>

      <div className='dashboard-content-profil-infos space-y-8'>
        <div className='space-y-8'>
          <div className='flex space-x-4'>
            <div className='w-1/2'>
              <label className='label'>Prénom :</label>
              <div className='input input-email'>
                <input
                  type='text'
                  placeholder={prenom}
                  className='input text-gray-400 cursor-not-allowed'
                  value={prenom || ''}
                  onChange={(e) => setPrenom(e.target.value)}
                  readOnly
                />
              </div>
            </div>
            <div className='w-1/2'>
              <label className='label'>Nom de famille :</label>
              <div className='input input-email'>
                <input
                  type='text'
                  placeholder={nom}
                  className='input text-gray-400 cursor-not-allowed'
                  value={nom || ''}
                  onChange={(e) => setNom(e.target.value)}
                  readOnly
                />
              </div>
            </div>
          </div>
        </div>

        <div className='space-y-8'>
          <div className='flex space-x-4'>
            <div className='w-1/2'>
              <label className='label'>Ville :</label>
              <div className='input input-email'>
                <input
                  type='text'
                  placeholder={city}
                  className='input'
                  value={city || ''}
                  onChange={async (e) => {
                    setCity(e.target.value)
                    const data = {
                      city: e.target.value,
                    }
                    await updateAccount(token, data)
                  }}
                />
              </div>
            </div>
            <div className='w-1/2'>
              <label className='label'>N ° de téléphone :</label>
              <div className='input input-email'>
                <input
                  type='text'
                  placeholder={telephone}
                  className='input'
                  value={telephone || ''}
                  onChange={async (e) => {
                    setTelephone(e.target.value)
                    const data = {
                      phoneNumber: e.target.value,
                    }
                    await updateAccount(token, data)
                  }}
                />
              </div>
            </div>
          </div>
        </div>

        <div className='space-y-8'>
          <div className='flex space-x-4 pr-10 justify-between items-end'>
            <div className='w-1/2'>
              <label className='label'>Date de naissance :</label>
              <div className='input input-email'>
                <input
                  type='date'
                  placeholder={dateNaissance}
                  className='input'
                  value={new Date(dateNaissance).toISOString().split('T')[0] || ''}
                  onChange={async (e) => {
                    setDateNaissance(e.target.value)
                    const data = {
                      birthdate: e.target.value,
                    }

                    await updateAccount(token, data)
                  }}
                />
              </div>
            </div>
            <div className='flex items-center'>
              <label htmlFor='toggle' className='flex items-center cursor-pointer'>
                <div className='relative'>
                  <input
                    type='checkbox'
                    id='toggle'
                    className='sr-only'
                    checked={isAvailable}
                    onChange={handleToggle}
                  />
                  <div
                    className={`block w-10 h-6 rounded-full ${
                      isAvailable ? 'bg-orange-600' : 'bg-orange-200'
                    }`}
                  ></div>
                  <div
                    className={`dot absolute left-1 top-1 bg-white w-4 h-4 rounded-full transition-transform ${
                      isAvailable ? 'transform translate-x-full' : ''
                    }`}
                  ></div>
                </div>
                <div className='ml-3'>
                  <span className={`block text-sm ${isAvailable ? 'text-black' : 'text-gray-400'}`}>
                    Disponibilité immédiate
                  </span>
                </div>
              </label>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default withAuth(Profil)
