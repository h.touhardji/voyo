export interface FormValuesType {
  nom: string
  prenom: string
  civilite: string
  dateDeNaissance: string
  villeDeNaissance: string
  adresse: string
  villesVisisteur: string[]
  codePostal: string
  telephone: string
  email: string
  nbAvis: number
  note: number
  parlezDeVous: string
  disponibleImmediatement: boolean
  tarif: number
  delaiAcceptation: number
  preferenceModification: string
  preferenceAnnulation: string
}
