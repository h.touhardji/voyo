import React, { useState, useRef, useEffect } from 'react'
import logo from '../../assets/images/logo_final/logo.png'
import './Menu.css'
import { Person, Search } from '@mui/icons-material'
import etourneau from '../../assets/images/image_oiseaux/etourneau.png'
interface MenuProps {
  authState: {
    email: string
    firstname: string
    lastname: string
    phoneNumber: string
    createdAt: Date
    updatedAt: Date
    userImg: string
    birthdate: string
    city: string
    isVisiteur: boolean
  }
  authProps: {
    isLoggedIn: boolean
    setIsLoggedIn: React.Dispatch<React.SetStateAction<boolean>>
  }
}

const Menu: React.FC<MenuProps> = ({ authState, authProps }) => {
  const { isLoggedIn, setIsLoggedIn } = authProps
  const [menuOpen, setMenuOpen] = useState(false)
  const [isScrolling, setIsScrolling] = useState(false)
  const [isProfileVisible, setIsProfileVisible] = useState(false) // État pour la visibilité du profil
  const profileCardRef = useRef<HTMLDivElement>(null)

  const toggleMenu = () => {
    setMenuOpen(!menuOpen)
  }

  const toggleProfile = () => {
    setIsProfileVisible(!isProfileVisible)
  }

  const handleScroll = () => {
    const scrollPosition = window.scrollY
    const windowHeight = window.innerHeight
    if (scrollPosition > windowHeight * 0.1) {
      setIsScrolling(true)
    } else {
      setIsScrolling(false)
    }
  }

  useEffect(() => {
    window.addEventListener('scroll', handleScroll)
    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
  }, [])

  return (
    <header
      className={`fixed z-10 py-1 w-11/12 px-5 flex justify-between top-0 left-1/2 transform -translate-x-1/2 ${
        menuOpen ? 'active' : ''
      } ${isScrolling ? 'shadow-xl border border-orange-100 bg-white mt-5 rounded-2xl py-2' : ''}`}
    >
      <div></div>
      <div className='flex items-center'>
        <div className=''>
          <a href='/'>
            <img src={logo} alt='Logo' className={`${isScrolling ? 'w-20' : 'w-32'}`} />
          </a>
        </div>

        <input
          className='menu-btn hidden'
          type='checkbox'
          id='menu-btn'
          checked={menuOpen}
          onChange={toggleMenu}
        />
        <label className='menu-icon' htmlFor='menu-btn'>
          <span className='navicon'></span>
        </label>
      </div>

      <ul className={`menu ${menuOpen ? 'open' : ''}`}>
        {isLoggedIn ? (
          <ul>
            <li>
              <a href='/programmation-visite' className='reset-active menu-li-hover'>
                <Search /> Rechercher un visiteur
              </a>
            </li>
            {!authState.isVisiteur && (
              <li>
                <a href='/inscription-visiteur' className='menu-li-hover'>
                  <Person /> Devenir visiteur
                </a>
              </li>
            )}
            <div className='user-picture-content'>
              <a href='#' className='user-picture' onClick={toggleProfile}>
                <img src={authState.userImg ? authState.userImg : etourneau} alt='' />
              </a>

              <div
                className={`user-picture-info ${isProfileVisible ? '' : 'hidden'}`}
                ref={profileCardRef}
              >
                {authState.isVisiteur && (
                  <li>
                    <a href='/profil-visiteur' className='reset-active menu-li-hover'>
                      Mes paramètres
                    </a>
                  </li>
                )}
                <li>
                  <a href='/programmation-visite' className='menu-li-hover'>
                    Planifier une visite
                  </a>
                </li>
                <li>
                  <a href='/dashboard' className='menu-li-hover'>
                    Dashboard
                  </a>
                </li>
              </div>
            </div>

            <li>
              <button
                className='start-button bg-orange'
                onClick={() => {
                  localStorage.removeItem('token')
                  setIsLoggedIn(false)
                  window.location.reload()
                }}
              >
                Déconnexion
              </button>
            </li>
          </ul>
        ) : (
          <li>
            <a href='/inscription'>
              <button className='start-button bg-orange'>Commencer</button>
            </a>
          </li>
        )}
      </ul>
    </header>
  )
}

export default Menu
