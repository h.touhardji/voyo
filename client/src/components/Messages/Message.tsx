// Message.tsx
import React, { useState } from 'react'
import classNames from 'classnames'

interface MessageProps {
  type: 'success' | 'info' | 'alert'
  message: string
}

const Message: React.FC<MessageProps> = ({ type, message }) => {
  const [isVisible, setIsVisible] = useState(true)

  const handleClose = () => {
    setIsVisible(false)
  }

  return (
    <>
      {isVisible && (
        <div
          className={classNames('absolute inset-0 flex items-center justify-center', {
            'bg-green-500': type === 'success',
            'bg-blue-500': type === 'info',
            'bg-red-500': type === 'alert',
          })}
          style={{ backgroundColor: 'rgba(0, 0, 0, 0.5)' }}
        >
          <div className='p-4 text-white max-w-md rounded shadow-md'>
            <button onClick={handleClose} className='float-right focus:outline-none'>
              X
            </button>
            <p>{message}</p>
          </div>
        </div>
      )}
    </>
  )
}

export default Message
