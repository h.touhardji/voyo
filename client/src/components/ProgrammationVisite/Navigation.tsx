import React, { useEffect, useState } from 'react'
import './Navigation.css'

interface Props {
  components: React.ReactNode[]
  handleFormSubmit: () => void
  setMissingValue: React.Dispatch<React.SetStateAction<boolean>>
  missingValue: boolean
  validation: [
    formDataValidPart1: {
      date: boolean
      heureVisite: boolean
    },
    formDataValidPart2: {
      proprietaire: boolean
      adresseBien: boolean
      ville: boolean
      codePostal: boolean
      typeBien: boolean
    },
    formDataValidPart3: {
      selectedProfile: boolean
    },
  ]
}

const Navigation: React.FC<Props> = ({
  components,
  handleFormSubmit,
  validation,
  setMissingValue,
  missingValue,
}) => {
  const [currentStep, setCurrentStep] = useState(0)
  const totalSteps = components.length

  // Tableau d'étapes, chaque élément est un objet avec des propriétés 'active' et 'done'
  const [steps, setSteps] = useState<Array<{ active: boolean; done: boolean }>>(
    Array(totalSteps).fill({ active: false, done: false }),
  )

  const stepNames = [
    'Planifiez votre visite',
    'Ajouter des informations',
    'Choisissez un visiteur',
    'Planifier une visite',
  ]

  const markStepAsDone = (stepIndex: number) => {
    const updatedSteps = steps.map((step, index) => ({
      active: index === stepIndex,
      done: index < stepIndex,
    }))
    setSteps(updatedSteps)
  }

  useEffect(() => {
    markStepAsDone(currentStep)
  }, [currentStep])

  const nextStep = () => {
    const valeurs = Object.values(validation[currentStep])
    const value = valeurs.every((valeur) => valeur)
    if (!value) {
      setMissingValue(true)
    } else {
      setMissingValue(false)
      if (currentStep < totalSteps - 1) {
        setCurrentStep(currentStep + 1)
      }
    }
  }

  const prevStep = () => {
    setMissingValue(false)
    if (currentStep > 0) {
      setCurrentStep(currentStep - 1)
    }
  }

  const isLastStep = currentStep === totalSteps - 1

  // Fonction pour calculer les classes CSS en fonction de l'état de l'étape
  const getStepClasses = (stepIndex: number) => {
    const step = steps[stepIndex]
    const classes = ['dots-steps']

    if (step.active) {
      classes.push('dots-steps-active')
    } else if (step.done) {
      classes.push('dots-steps-done')
    }

    return classes.join(' ')
  }

  return (
    <div className=''>
      <ul className='steps'>
        {components.map((_, index) => {
          // activer le code si on est sur un telephone ou une tablette
          const isActive = steps[index].active
          const isMobileOrTablet = window.innerWidth <= 768

          // Vérifiez si la classe .dots-steps est active avant d'afficher l'élément <li>
          if (!isActive && isMobileOrTablet) {
            return null // Retourne null pour masquer l'élément <li>
          }

          return (
            <li key={index}>
              <span className={getStepClasses(index)}>{index + 1}</span>
              {stepNames[index]}
            </li>
          )
        })}
      </ul>

      <div className=''>{React.cloneElement(components[currentStep] as React.ReactElement)}</div>
      {missingValue && (
        <div className='programmation-visite-error-message'>
          Merci de completer l&apos;ensemble des champs{' '}
        </div>
      )}
      <div className='btn-navigation'>
        {currentStep > 0 && (
          <button className='bg-tranparent' onClick={prevStep}>
            Précédent
          </button>
        )}
        <button
          className={`bg-orange ${isLastStep ? '' : ''}`}
          onClick={isLastStep ? handleFormSubmit : nextStep}
        >
          {isLastStep ? 'Terminer' : 'Suivant'}
        </button>
      </div>
    </div>
  )
}

export default Navigation
