import { ProfileVisiteurType } from './ProfilVisiteur.type'

export type ProfilType = {
  id: number
  userImg: string
  description: string
  visiteur: string
  createdAt: Date
  updatedAt: Date
  email: string
  isProfileVisiteur: boolean
  profileVisiteur: ProfileVisiteurType
}
