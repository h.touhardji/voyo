export type ProfileVisiteurType = {
  profileId: number
  nom: string
  prenom: string
  civilite: string
  nbAvis: string
  dateDeNaissance: Date
  villeDeNaissance: string
  adressePostale: string
  codePostal: string
  telephone: string
  email: string
  parlezDeVous: string
  disponibleImmediatement: boolean
  createdAt: Date
  updatedAt: Date
  tarif: number
  note: number
  delaiAcceptation: string
  preferenceModification: string
  preferenceAnnulation: string
}
