import React, { useState } from 'react'
import './index.css' // Assurez-vous que le chemin du fichier CSS est correct.
import Navigation from './Navigation'
import chicken from '../../assets/images/chick.png'
import Menu from '../Menu/Menu'
import Footer from '../Footer/Footer'
import Part4 from './part4/Part4'
import Part3 from './part3/Part3'
import Part2 from './part2/Part2'
import Part1 from './part1/Part1'
import { createVisite } from '../../api/Visites'
import { ProfilType } from './Profil.type'
import { convertDate } from '../../utils/Data.utils'
import swal from 'sweetalert'
import withAuth from '../Security/withAuth'
interface ProgrammationVisiteProps {
  authState: {
    email: string
    firstname: string
    lastname: string
    phoneNumber: string
    createdAt: Date
    updatedAt: Date
    userImg: string
    birthdate: string
    city: string
    isVisiteur: boolean
  }
  authProps: {
    token: string
    isLoggedIn: boolean
    setIsLoggedIn: React.Dispatch<React.SetStateAction<boolean>>
  }
}

const ProgrammationVisite: React.FC<ProgrammationVisiteProps> = ({
  authState,
  authProps,
}: ProgrammationVisiteProps) => {
  const [displayChick, setDisplayChick] = useState(false)
  const [missingValue, setMissingValue] = useState(false)
  const [formData, setFormData] = useState<{
    date: Date
    heureVisite: string
    proprietaire: string
    adresseBien: string
    ville: string
    codePostal: string
    typeBien: string
    prix: number
    informationsComplementaires: string
    selectedProfile: ProfilType
  }>({
    date: new Date(),
    heureVisite: '',
    proprietaire: '',
    adresseBien: '',
    ville: '',
    codePostal: '',
    typeBien: '',
    prix: 0,
    informationsComplementaires: 'string',
    selectedProfile: {
      id: 0,
      userImg: '',
      description: '',
      visiteur: '',
      createdAt: new Date(),
      updatedAt: new Date(),
      email: '',
      isProfileVisiteur: false,
      profileVisiteur: {
        profileId: 0,
        nom: '',
        prenom: '',
        nbAvis: '',
        civilite: '',
        dateDeNaissance: new Date(),
        villeDeNaissance: '',
        adressePostale: '',
        codePostal: '',
        telephone: '',
        email: '',
        parlezDeVous: '',
        disponibleImmediatement: false,
        createdAt: new Date(),
        updatedAt: new Date(),
        tarif: 0,
        note: 0,
        delaiAcceptation: '',
        preferenceModification: '',
        preferenceAnnulation: '',
      },
    },
  })
  const { token } = authProps

  const [formDataValidPart1, setFormDataValidPart1] = useState({
    date: false,
    heureVisite: false,
  })
  const [formDataValidPart2, setFormDataValidPart2] = useState({
    proprietaire: false,
    adresseBien: false,
    ville: false,
    codePostal: false,
    typeBien: false,
  })
  const [formDataValidPart3, setFormDataValidPart3] = useState({
    selectedProfile: false,
  })

  const componentsToRender = [
    <Part1
      key='1'
      setFormData={setFormData}
      formData={formData}
      setFormDataValid={setFormDataValidPart1}
      formDataValid={formDataValidPart1}
    />,
    <Part2
      key='2'
      setFormData={setFormData}
      formData={formData}
      setFormDataValid={setFormDataValidPart2}
      formDataValid={formDataValidPart2}
    />,
    <Part3
      key='3'
      setFormData={setFormData}
      formData={formData}
      setFormDataValid={setFormDataValidPart3}
      formDataValid={formDataValidPart3}
      token={authProps.token}
    />,
    <Part4 key='3' setFormData={setFormData} formData={formData} />,
  ]

  const handleFormSubmit = async () => {
    try {
      if (formData) {
        const visite = {
          date: formData.date,
          heureVisite: formData.heureVisite,
          proprietaire: formData.proprietaire,
          adresseBien: formData.adresseBien,
          ville: formData.ville,
          codePostal: formData.codePostal,
          typeBien: formData.typeBien,
          visiteurEmail: formData.selectedProfile.email,
          prix: formData.prix,
          informationsComplementaires: formData.informationsComplementaires,
        }
        console.log(formData.selectedProfile.profileVisiteur.prenom)

        const response = await createVisite(token, visite)

        if (response.success) {
          setDisplayChick(true)
        } else {
          swal('Erreur ! Une erreur est survenue dans votre demande de visite !', {
            icon: 'error',
          })
        }
      }
    } catch (error) {
      console.error('Erreur lors de la création du profil visiteur', error)
    }
  }

  return (
    <div>
      {displayChick && (
        <div className='chicken-message'>
          <div className='chicken-message-filtre'>
            <div className='chicken-message-content text-center p-8 bg-white border border-gray-300 rounded-md shadow-lg'>
              <img src={chicken} alt='chicken' className='mx-auto mb-4' />
              <h2 className='text-xl font-bold mb-2'>Super !</h2>
              <p className='text-gray-700'>
                {formData.selectedProfile.profileVisiteur.prenom} vous répondra dans les plus brefs
                délais.
              </p>
              <p className='text-gray-700 text-xs'>
                Nous vous invitons à vous rendre dans votre dashboard pour continuer l&apos;échange.
              </p>
              <a
                href='/'
                className='btn mt-4 bg-orange inline-block px-6 py-2 rounded-full text-white'
              >
                J&apos;ai compris
              </a>
            </div>
          </div>
        </div>
      )}
      <Menu authState={authState} authProps={authProps} />
      <div className='programmation-visite'>
        <div className='programmation-visite-titre-container'>
          <h1 className='programmation-visite-titre'>
            Programmer la visite d&apos;un bien par un de nos visiteurs
          </h1>
          <svg
            xmlns='http://www.w3.org/2000/svg'
            width='187'
            height='84'
            viewBox='0 0 187 84'
            fill='none'
          >
            <path
              d='M154.556 6.23943C120.112 -0.524946 43.0276 -4.1608 10.2418 35.4108C-30.7403 84.8753 92.0328 86.1848 138.769 78.5337C166.733 73.9558 185 62.755 185 46.5498C185 32.5982 153.281 11.6298 95.8738 15.5933'
              stroke='url(#paint0_linear_105_2012)'
              strokeWidth='4'
              strokeLinecap='round'
            />
            <defs>
              <linearGradient
                id='paint0_linear_105_2012'
                x1='2'
                y1='2'
                x2='131.982'
                y2='134.149'
                gradientUnits='userSpaceOnUse'
              >
                <stop stopColor='#F40000' />
                <stop offset='1' stopColor='#F75000' />
              </linearGradient>
            </defs>
          </svg>
        </div>
        <Navigation
          components={componentsToRender}
          validation={[formDataValidPart1, formDataValidPart2, formDataValidPart3]}
          handleFormSubmit={handleFormSubmit}
          missingValue={missingValue}
          setMissingValue={setMissingValue}
        />
      </div>
      <Footer />
    </div>
  )
}

export default withAuth(ProgrammationVisite)
