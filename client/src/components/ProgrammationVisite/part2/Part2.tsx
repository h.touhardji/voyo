import React from 'react'
import { ProfilType } from '../Profil.type'

interface FormDataProps {
  formData: {
    date: Date
    heureVisite: string
    proprietaire: string
    adresseBien: string
    ville: string
    codePostal: string
    typeBien: string
    prix: number
    informationsComplementaires: string
    selectedProfile: ProfilType
  }
  setFormData: React.Dispatch<
    React.SetStateAction<{
      date: Date
      heureVisite: string
      proprietaire: string
      adresseBien: string
      ville: string
      prix: number
      informationsComplementaires: string
      codePostal: string
      typeBien: string
      selectedProfile: ProfilType
    }>
  >
  formDataValid: any
  setFormDataValid: React.Dispatch<React.SetStateAction<any>>
}

export default function Part2({
  formData,
  setFormData,
  formDataValid,
  setFormDataValid,
}: FormDataProps) {
  const updatedFormDataValid = { ...formDataValid } // Créez une copie de formDataValid pour éviter la mutation directe

  // Champ "Propriétaire"
  const handleProprietaireChange = (e: { target: { value: any } }) => {
    const proprietaire = e.target.value
    const isProprietaireValid = true // Remplacez ceci par votre logique de validation

    updatedFormDataValid.proprietaire = isProprietaireValid // Assurez-vous de mettre à jour le champ approprié dans formDataValid
    setFormDataValid(updatedFormDataValid)

    setFormData({ ...formData, proprietaire })
  }

  // Champ "Adresse du bien"
  const handleAdresseBienChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const adresseBien = e.target.value
    const isAdresseBienValid = true // Remplacez ceci par votre logique de validation

    updatedFormDataValid.adresseBien = isAdresseBienValid
    setFormDataValid(updatedFormDataValid)

    setFormData({ ...formData, adresseBien })
  }

  // Champ "Ville"
  const handleVilleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const ville = e.target.value
    const isVilleValid = true // Remplacez ceci par votre logique de validation

    updatedFormDataValid.ville = isVilleValid
    setFormDataValid(updatedFormDataValid)

    setFormData({ ...formData, ville })
  }

  // Champ "Code postal"
  const handleCodePostalChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const codePostal = e.target.value
    const isCodePostalValid = true // Remplacez ceci par votre logique de validation

    updatedFormDataValid.codePostal = isCodePostalValid
    setFormDataValid(updatedFormDataValid)

    setFormData({ ...formData, codePostal })
  }

  // Champ "Type de bien"
  const handleTypeBienChange = (e: { target: { value: any } }) => {
    const typeBien = e.target.value
    const isTypeBienValid = true // Remplacez ceci par votre logique de validation

    updatedFormDataValid.typeBien = isTypeBienValid
    setFormDataValid(updatedFormDataValid)

    setFormData({ ...formData, typeBien })
  }

  return (
    <div>
      <h2 className='programmation-visite-sous-titre'>Planifiez votre visite</h2>
      <div className='part-content'>
        <div className='my-4'>
          <label className='block text-sm font-medium text-gray-600'>Propriétaire :</label>
          <select
            value={formData.proprietaire}
            onChange={handleProprietaireChange}
            className='mt-1 p-2 border rounded-md w-full'
          >
            <option value='' disabled hidden>
              Sélectionnez le type
            </option>
            <option value='particulier'>Particulier</option>
            <option value='agence'>Agence</option>
          </select>
        </div>

        <div className='my-4'>
          <label className='block text-sm font-medium text-gray-600'>Adresse du bien :</label>
          <input
            type='text'
            onChange={handleAdresseBienChange}
            placeholder='Adresse du bien'
            className='mt-1 p-2 border rounded-md w-full'
          />
        </div>
      </div>
      <div className='part-content'>
        <div className='my-4'>
          <label className='block text-sm font-medium text-gray-600'>Ville :</label>
          <input
            type='text'
            onChange={handleVilleChange}
            placeholder='Ville'
            className='mt-1 p-2 border rounded-md w-full'
          />
        </div>

        <div className='my-4'>
          <label className='block text-sm font-medium text-gray-600'>Code postal :</label>
          <input
            type='text'
            onChange={handleCodePostalChange}
            placeholder='Code postal'
            className='mt-1 p-2 border rounded-md w-full'
          />
        </div>
      </div>
      <div className='part-content'>
        <div className='my-4'>
          <label className='block text-sm font-medium text-gray-600'>Type de bien :</label>
          <select
            value={formData.proprietaire}
            onChange={handleTypeBienChange}
            className='mt-1 p-2 border rounded-md w-full'
          >
            <option value='' disabled hidden>
              Sélectionnez le type
            </option>
            <option value='appartement'>Appartement</option>
            <option value='maison'>Maison</option>
          </select>
        </div>
      </div>
    </div>
  )
}
