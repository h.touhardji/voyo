import { CalendarMonth, LocationOn } from '@mui/icons-material'
import React, { useEffect } from 'react'
import etourneau from '../../../assets/images/image_oiseaux/etourneau.png'
import birdJaune from '../../../assets/images/bird-jaune.png'
import { ProfilType } from '../Profil.type'
import { convertDate } from '../../../utils/Data.utils'
interface FormDataProps {
  formData: {
    date: Date
    heureVisite: string
    proprietaire: string
    adresseBien: string
    ville: string
    codePostal: string
    typeBien: string
    prix: number
    informationsComplementaires: string
    selectedProfile: ProfilType
  }
  setFormData: React.Dispatch<
    React.SetStateAction<{
      date: Date
      heureVisite: string
      proprietaire: string
      adresseBien: string
      ville: string
      codePostal: string
      typeBien: string
      prix: number
      informationsComplementaires: string
      selectedProfile: ProfilType
    }>
  >
}

export default function Part4({ formData, setFormData }: FormDataProps) {
  const profilUserSelected = formData.selectedProfile.profileVisiteur

  const fraisService = 5
  useEffect(() => {
    setFormData({ ...formData, prix: profilUserSelected.tarif + fraisService })
  }, [])
  return (
    <div>
      <h2 className='programmation-visite-sous-titre'>Planifier une visite</h2>
      <div>
        <div className='recap-programmation-visite'>
          <div className='recap-programmation-visite-gauche'>
            <div className='recap-programmation-profil'>
              <img
                src={
                  formData.selectedProfile.userImg ? formData.selectedProfile.userImg : etourneau
                }
                alt=''
              />
              <div className='recap-programmation-profil-info'>
                <div className='recap-programmation-profil-name'>
                  <p>{profilUserSelected.nom}</p>
                  <p>{profilUserSelected.prenom}</p>
                </div>
                {profilUserSelected.note && (
                  <p>
                    {profilUserSelected.note} <span>({profilUserSelected.nbAvis} avis)</span>
                  </p>
                )}
              </div>
            </div>
            <hr />
            <div className='recap-programmation-infos'>
              <h4>Résumé de votre demande</h4>
              <div>
                <CalendarMonth /> <p>{convertDate(formData.date)}</p>
              </div>
              <div>
                <LocationOn />
                <p>
                  {formData.ville}, {formData.codePostal}
                </p>
              </div>
            </div>
            <hr />
            <div className='recap-programmation-autre'>
              <h4>Informations complémentaires demandées</h4>
              <textarea
                onChange={(e) => {
                  const informationsComplementaires = e.target.value
                  setFormData({ ...formData, informationsComplementaires })
                }}
                placeholder="Ce champ est destiné au premier message envoyé au visiteur. N'hésitez pas à lui
                indiquer la surface à visiter, s'il y a un jardin et tout autre information
                importante"
              ></textarea>
            </div>
          </div>
          <div className='recap-programmation-droite'>
            <div className='recap-programmation-date'>
              <h2>Votre visite du {convertDate(formData.date)}</h2>
              <img src={birdJaune} alt='' />
            </div>
            <div className='recap-programmation-prix'>
              <h4>Prix détaillé</h4>
              <div>
                <p>Prix visiteur</p>
                <p>{profilUserSelected.tarif} €</p>
              </div>
              <div>
                <p>Frais de service</p>
                <p>{fraisService} €</p>
              </div>
              <hr />
              <div className='recap-programmation-prix-total'>
                <p>Total</p>
                <h2>{profilUserSelected.tarif + fraisService} €</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
