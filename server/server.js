const express = require("express");
const cors = require("cors");
const db = require("./src/models");
const cookieSession = require("cookie-session");
const passport = require("passport");
const app = express();
const http = require("http").Server(app);

const socketIO = require("socket.io")(http, {
  cors: {
    origin: ["http://localhost:3000", "https://voyo.fr/"],
  },
});

const { join } = require("path");

const PORT = 8800;

const dotenv = require("dotenv");

// Déterminez l'environnement actuel
const environment = process.env.NODE_ENV || "development";

// Chargez les variables d'environnement en fonction de l'environnement
if (environment === "development") {
  dotenv.config({ path: ".env.development" });
  console.log("utilise .env.development");
} else if (environment === "production") {
  console.log("utilise .env.production");
  dotenv.config({ path: ".env.production" });
}

// Apply Application Middlewares
app.use(
  cors({
    origin: "*",
    credentials: true,
    methods: "GET,POST,PUT,DELETE",
  })
);
app.use(passport.initialize());
app.use(express.static(join(__dirname, "./src/")));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
app.use(
  cookieSession({
    name: "session",
    keys: ["key1", "key2"],
    maxAge: 24 * 60 * 60 * 1000, // durée de validité du cookie en millisecondes
  })
);

// Import passport middleware
require("./src/middlewares/passport-middleware");

// MDP
// Password123#@!
app.use(passport.initialize());
app.use(passport.session());

// database
const Role = db.Roles;

// Routers
const visitesRouter = require("./src/routes/Visites");
app.use("/api/visites", visitesRouter);
const usersRouter = require("./src/routes/Users");
app.use("/api/users", usersRouter);
const profilesRouter = require("./src/routes/Profile");
app.use("/api/profiles", profilesRouter);
const profileVisiteurRouter = require("./src/routes/ProfileVisiteur");
app.use("/api/profileVisiteur", profileVisiteurRouter);
const postsRouter = require("./src/routes/Posts");
app.use("/api/posts", postsRouter);
const disponibilitesRouter = require("./src/routes/Disponibilites");
app.use("/api/disponibilites", disponibilitesRouter);
const googleRouter = require("./src/routes/GoogleAuth");
app.use("/api/auth", googleRouter);
const chatRouter = require("./src/routes/Chat");
app.use("/api/chat", chatRouter);
const avisRouter = require("./src/routes/Avis");
app.use("/api/avis", avisRouter);
const villesVisiteurRouter = require("./src/routes/VillesVisiteur");
app.use("/api/villesVisiteur", villesVisiteurRouter);
const messageRouter = require("./src/routes/Message");
app.use("/api/message", messageRouter);
const elementVisiteRouter = require("./src/routes/ElementVisite");
app.use("/api/element-visite", elementVisiteRouter);
app.get("/api/", (req, res) => {
  res.status(200).send({ message: "Server is started" });
});

// Endpoint pour vider toutes les données de toutes les tables
app.delete("/api/reset-database", async (req, res) => {
  try {
    await db.sequelize.sync({ force: true });
    res.status(200).json({ message: "Toutes les données ont été supprimées." });
  } catch (error) {
    console.error(
      "Erreur lors de la réinitialisation de la base de données :",
      error
    );
    res.status(500).json({
      error: "Erreur lors de la réinitialisation de la base de données.",
    });
  }
});

// socket.io
let onlineUsers = [];
const ioNamespace = socketIO.of("/socket"); // Create a namespace for "/socket"

ioNamespace.on("connection", (socket) => {
  socket.on("message", (data) => {
    ioNamespace.emit("messageResponse", data);
  });

  socket.on("newUser", (userEmail) => {
    !onlineUsers.some((user) => user.userId === userEmail) &&
      onlineUsers.push({ userId: userEmail, socketID: socket.id });
    console.log("--------------------");
    onlineUsers.map((user) => {
      console.log(`User ID: ${user.userId}, Socket ID: ${user.socketID}`);
    });
    console.log("--------------------");
    ioNamespace.emit("onlineUsersUpdate", onlineUsers);
  });

  socket.on("typing", (data) => socket.broadcast.emit("typingResponse", data));

  socket.on("disconnect", () => {
    // Supprimez l'utilisateur déconnecté de la liste
    onlineUsers = onlineUsers.filter((user) => user.socketID !== socket.id);
    console.log("🔥: A user disconnected");

    // Émettez la liste mise à jour à tous les utilisateurs connectés
    ioNamespace.emit("onlineUsersUpdate", onlineUsers);
    socket.disconnect();
  });
});

db.sequelize.sync().then(() => {
  http.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
  });
});

// await db.sequelize.query("SET FOREIGN_KEY_CHECKS=0;");
// db.sequelize.query("DROP TABLE IF EXISTS `chat`;");
// db.sequelize.query("SET FOREIGN_KEY_CHECKS=1;");

// db.sequelize.sync({ force: true }).then(() => {
//   app.listen(PORT, () => {
//     console.log(`Server is running on port ${PORT}.`);
//   });
// });
