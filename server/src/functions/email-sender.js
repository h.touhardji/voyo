const nodemailer = require("nodemailer");

const EMAIL = process.env.EMAIL;
const PASSWORD_EMAIL = process.env.PASSWORD_EMAIL;

/** Envoi d'un e-mail depuis un véritable compte Gmail */
const sendEmailMessage = (email_address, email_subject, body_html) => {
  return new Promise((resolve, reject) => {
    try {
      // Configuration du transporteur (transporter)
      let transporter = nodemailer.createTransport({
        host: "smtp.ionos.fr",
        port: 465,
        secure: true,
        auth: {
          user: EMAIL,
          pass: PASSWORD_EMAIL,
        },
      });

      // Corps de l'e-mail
      let mailOptions = {
        from: EMAIL,
        to: email_address,
        subject: email_subject,
        html: body_html,
      };

      // Envoi de l'e-mail
      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          console.error(
            "Une erreur s'est produite lors de l'envoi de l'e-mail",
            error
          );
          reject("Une erreur s'est produite lors de l'envoi de l'e-mail");
        } else {
          console.log("E-mail envoyé :", info.response);
          resolve(info);
        }
      });
    } catch (err) {
      console.error(
        "Une erreur s'est produite lors de l'envoi de l'e-mail",
        err
      );
      reject("Une erreur s'est produite lors de l'envoi de l'e-mail");
    }
  });
};

module.exports = sendEmailMessage;
