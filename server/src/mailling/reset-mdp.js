const mailBodyModifierMotDePasse = (link) => {
	const mail = `
    <!DOCTYPE html>
    <html lang="fr">
        <head>
            <meta charset="UTF-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <title>Générer un nouveau mot de passe</title>
            <style>
                /* Styles CSS pour le corps de l'e-mail */
                body,
                *,
                html {
                    font-family: Arial, sans-serif;
                    line-height: 1.6;
                    margin: 0;
                    padding: 0;
                }

                .container {
                    background-color: #f4f4f4;
                    max-width: 600px;
                    margin: 0 auto;
                    padding: 2%;
                    justify-content: space-between;
                    min-height: 80vh;
                }

                .header {
                    text-align: center;
                    padding: 1%;
                }

                .header > img {
                    width: 20%;
                }

                .content {
                    padding: 2%;
                }

                .button {
                    display: inline-block;
                    color: #fff;
                    padding: 1% 2%;
                    text-decoration: none;
                    border-radius: 5px;
                }

                .footer {
                    text-align: center;
                    padding: 1%;
                }

                .footer a {
                    color: gray;
                }

                .connexion {
                    text-decoration: none;
                    background: linear-gradient(114deg, #f40000 0%, #f75000 100%);
                    color: #ffffff !important;
                    cursor: pointer;
                    display: flex;
                    padding: 3% 10%;
                    justify-content: center;
                    align-items: center;
                    border-radius: 8px;
                    font-size: 16px;
                    font-style: normal;
                    font-weight: 700;
                    line-height: normal;
                    width: fit-content;
                    margin: auto;
                    margin-top: 50px;
                    margin-bottom: 50px;
                }

                .footer a i {
                    font-size: 24px;
                    background: #f75000;
                    border-radius: 50%;
                    padding: 1.5%;
                    margin: 10% 3%;
                    color: white;
                    text-align: center;
                    display: inline-block;
                    transition: background-color 0.3s;
                }

                .footer a i:hover {
                    background: #f40000;
                }

                .text-italique {
                    font-style: italic;
                    text-align: center;
                    margin: auto;
                    color: gray;
                    padding: 2%;
                    margin-top: 5%;
                }
                @media screen and (max-width: 600px) {
                    .container {
                        width: 90%;
                        padding: 0;
                        margin: 0;
                    }

                    .button {
                        display: block;
                        width: 100%;
                        text-align: center;
                    }
                }
            </style>
        </head>
        <body>
            <div class="container">
                <div class="header">
                    <img src="https://voyo.fr/logo.png" alt="logo" />
                </div>
                <div class="content">
                    <p>
                        Bonjour, <br />
                        Vous avez oublié votre mot de passe, <br /><br />
                        Voici le lien pour créer votre nouveau mot de passe et accéder à votre
                        compte<br />
                        Attention, ce lien est valable<b> 30 minutes</b>
                    </p>
                    <a href="${link}" class="connexion"
                        >Réinitialiser</a
                    >
                    <p>
                        N'hesiter pas a nous contacter , si vous rencontrer le moindre
                        probleme, a l'adresse suivante
                        <a href="hello@voyo.fr">hello@voyo.fr</a>
                        Notre equipe sera ravie de vous aider
                    </p>
                    <br />
                    <p>À bientôt,<br />Votre équipe Voyo</p>
                </div>
                <p class="text-italique">
                    Merci d'ajouter hello@voyo.fr à votre carnet d'adresse afin de vous
                    assurer de bien recevoir nos emails dans votre boite de réception
                </p>
                <div class="footer">
                    <p>
                        <a href="https://voyo.fr">Conditions d'utilisation</a> |
                        <a href="https://voyo.fr">Politique de confidentialité</a> |
                        <a href="https://voyo.fr">Notre blog</a> |
                        <a href="https://voyo.fr"">Contactez-nous</a>
                    </p>
                </div>
            </div>
        </body>
    </html>
    `;
	return mail;
};

module.exports = mailBodyModifierMotDePasse;
