const jwt = require('jsonwebtoken');

const validateToken = (req, res, next) => {
  // Vérifier si le token est inclus dans le header sous forme de "Bearer <token>"
  const authHeader = req.headers.authorization;

  if (!authHeader) {
    return res.status(403).json({ message: "No token provided!" });
  }

  // Extraire le token en supprimant le préfixe "Bearer "
  const token = authHeader.split(' ')[1];

  if (!token) {
    return res.status(403).json({ message: "No token provided!" });
  }

  try {
    // Vérifier la validité du token en utilisant la clé secrète JWT_SECRET
    const decodedToken = jwt.verify(token, process.env.JWT_SECRET);
    // Stocker les informations décodées du token dans req.user
    req.user = decodedToken;
    // Passer au middleware suivant
    next();
  } catch (err) {
    return res.status(403).json({ message: 'Invalid token' });
  }
};

module.exports = validateToken;
