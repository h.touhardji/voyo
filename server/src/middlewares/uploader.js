const multer = require("multer");
const path = require("path");

const environment = process.env.NODE_ENV || "development";

// Chargez les variables d'environnement en fonction de l'environnement
let uploadDirectory = "";
if (environment === "development") {
  uploadDirectory = path.join(__dirname, "..", "uploads");
} else if (environment === "production") {
  // Dans le cas de la production, vous devriez avoir une structure de dossier différente sur votre serveur
  uploadDirectory = "/var/www/html/voyo/uploads";
}

const filename = (req, file, next) => {
  console.log("Filename: ", file.originalname);
  let lastIndexof = file.originalname.lastIndexOf(".");
  let ext = file.originalname.substring(lastIndexof);
  next(null, `img-${Date.now()}${ext}`);
};

const destination = (req, file, next) => {
  console.log("Destination: " + uploadDirectory);
  next(null, uploadDirectory);
};

const postImageDestination = (req, file, next) => {
  console.log("postImageDestination: " + uploadDirectory);
  next(null, path.join(uploadDirectory, "post-images"));
};

// Configuration pour le téléchargement d'images de post
const uploadPostImage = multer({
  storage: multer.diskStorage({ destination: postImageDestination, filename }),
});

// Configuration générale pour le téléchargement d'autres fichiers
const upload = multer({
  storage: multer.diskStorage({ destination, filename }),
});

module.exports = { uploadPostImage, upload };
