module.exports = (sequelize, DataTypes) => {
	const Avis = sequelize.define("Avis", {
		note: {
			type: DataTypes.INTEGER,
			allowNull: false,
		},
		commentaire: {
			type: DataTypes.STRING,
			allowNull: false,
		},
	});

	Avis.associate = (models) => {
		Avis.belongsTo(models.Users, {
			as: "donneur", // alias pour l'utilisateur qui donne l'avis
			foreignKey: "donneurId",
		});
		Avis.belongsTo(models.Users, {
			as: "receveur", // alias pour l'utilisateur qui reçoit l'avis
			foreignKey: "receveurId",
		});
	};

	return Avis;
};
