module.exports = (sequelize, DataTypes) => {
  const Chat = sequelize.define("Chat", {
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  });

  Chat.associate = (models) => {
    Chat.belongsTo(models.Users, {
      as: "User1",
      targetKey: "email",
    });

    Chat.belongsTo(models.Users, {
      as: "User2",
      targetKey: "email",
    });

    Chat.belongsTo(models.Visites, {
      foreignKey: "visiteId",
      as: "visite",
      onDelete: "CASCADE",
      allowNull: false,
    });
  };

  return Chat;
};
