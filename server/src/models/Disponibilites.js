module.exports = (sequelize, DataTypes) => {
  const Disponibilites = sequelize.define("Disponibilites", {
    dataDisponibilites: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    recurence: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    heureDebut: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    heureFin: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });

  Disponibilites.associate = (models) => {
    Disponibilites.belongsTo(models.Users, {
      foreignKey: 'email',
      targetKey: 'email',
      as: 'user',
    });
  };

  return Disponibilites;
};
