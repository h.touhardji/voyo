module.exports = (sequelize, DataTypes) => {
  const ElementVisite = sequelize.define("ElementVisites", {
    nom: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    commentaire: {
      type: DataTypes.STRING,
      allowNull: true,
    },
  });

  ElementVisite.associate = (models) => {
    ElementVisite.belongsTo(models.Visites, {
      foreignKey: "visiteId",
      targetKey: "id",
      as: "visite",
      onDelete: "CASCADE",
      allowNull: false,
    });
  };

  return ElementVisite;
};
