module.exports = (sequelize, DataTypes) => {
	const Message = sequelize.define("Message", {
		content: {
			type: DataTypes.TEXT,
			allowNull: false,
		},
	});

	// Définissez des associations pour lier le message au chat et à l'utilisateur
	Message.associate = (models) => {
		Message.belongsTo(models.Chat, {
			foreignKey: "chatId",
			onDelete: "CASCADE",
		});
		Message.belongsTo(models.Users, {
			foreignKey: "userEmail",
		});
	};

	return Message;
};
