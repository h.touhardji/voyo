module.exports = (sequelize, DataTypes) => {
	const Profile = sequelize.define("Profile", {
		userImg: {
			type: DataTypes.STRING,
			allowNull: true,
		},
		description: {
			type: DataTypes.TEXT,
			allowNull: true,
		},
		visiteur: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
		},
	});

	Profile.associate = (models) => {
		Profile.belongsTo(models.Users, {
			foreignKey: "email",
			targetKey: "email",
			as: "user",
			onDelete: "CASCADE",
		});
		Profile.hasOne(models.ProfileVisiteur, {
			foreignKey: "profileId",
			as: "profileVisiteur",
			onDelete: "CASCADE",
		});
	};

	return Profile;
};
