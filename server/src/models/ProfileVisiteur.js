module.exports = (sequelize, DataTypes) => {
  const ProfileVisiteur = sequelize.define("ProfileVisiteur", {
    profileId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      unique: true,
    },
    nom: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    prenom: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    civilite: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    date_de_naissance: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    ville_de_naissance: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    adresse_postale: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    code_postal: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    telephone: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    nbAvis: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    note: {
      type: DataTypes.DECIMAL(2, 1),
      allowNull: false,
    },
    parlez_de_vous: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    tarif: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    disponibleImmediatement: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    delai_acceptation: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    preference_modification: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    preference_annulation: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });

  ProfileVisiteur.associate = (models) => {
    ProfileVisiteur.belongsTo(models.Profile, {
      foreignKey: "profileId",
      as: "profile",
      onDelete: "CASCADE",
    });
  };
  return ProfileVisiteur;
};
