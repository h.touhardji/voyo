const { compare, hash } = require("bcryptjs");
const { randomBytes } = require("crypto");
const { sign } = require("jsonwebtoken");
const { pick } = require("lodash");

module.exports = (sequelize, DataTypes) => {
	const Users = sequelize.define(
		"Users",
		{
			firstname: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			lastname: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			birthdate: {
				type: DataTypes.DATE,
				allowNull: true,
			},
			city: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			password: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			googleId: {
				type: DataTypes.STRING,
			},
			userImg: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			email: {
				type: DataTypes.STRING,
				allowNull: false,
				unique: true,
				primaryKey: true,
			},
			phoneNumber: {
				type: DataTypes.STRING,
				unique: true,
			},
			createdAt: {
				type: DataTypes.DATE,
				allowNull: false,
				defaultValue: DataTypes.NOW,
			},
			updatedAt: {
				type: DataTypes.DATE,
				allowNull: false,
				defaultValue: DataTypes.NOW,
			},
			verified: {
				type: DataTypes.BOOLEAN,
				defaultValue: false,
			},
			verificationCode: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			resetPasswordToken: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			resetPasswordExpiresIn: {
				type: DataTypes.DATE,
				allowNull: true,
			},
		},
		{ timestamps: true }
	);

	Users.beforeCreate(async (user, options) => {
		if (user.changed("password")) {
			user.password = await hash(user.password, 10);
		}
	});

	Users.associate = (models) => {
		Users.hasMany(models.Visites, {
			onDelete: "cascade",
		});

		Users.hasMany(models.Disponibilites, {
			foreignKey: "email",
			as: "disponibilites",
		});

		Users.hasOne(models.Profile, {
			foreignKey: "email",
			as: "profile",
		});
	};

	// Méthode pour générer le jeton de réinitialisation de mot de passe
	Users.prototype.generatePasswordReset = function () {
		this.resetPasswordExpiresIn = new Date(Date.now() + 36000000);
		this.resetPasswordToken = randomBytes(20).toString("hex");
	};

	// Méthode pour vérifier le mot de passe
	Users.prototype.comparePassword = async function (password) {
		return await compare(password, this.password);
	};

	// Méthode pour générer un jeton JWT
	Users.prototype.generateJWT = function () {
		const payload = {
			username: this.username,
			email: this.email,
			name: this.name,
			id: this._id,
		};
		return sign(payload, process.env.JWT_SECRET, { expiresIn: "1 day" });
	};

	// Méthode pour obtenir les informations de l'utilisateur
	Users.prototype.getUserInfo = function () {
		return pick(this, ["id", "firstname", "lastname", "email", "verified"]);
	};
	return Users;
};
