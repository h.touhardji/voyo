module.exports = (sequelize, DataTypes) => {
  const VilleVisiteur = sequelize.define("VilleVisiteur", {
    visiteurId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    ville: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });

  VilleVisiteur.associate = (models) => {
    VilleVisiteur.belongsTo(models.ProfileVisiteur, {
      foreignKey: "visiteurId",
      as: "visiteur",
      onDelete: "CASCADE",
    });
  };

  return VilleVisiteur;
};
