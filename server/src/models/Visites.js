module.exports = (sequelize, DataTypes) => {
  const Visite = sequelize.define("Visites", {
    date: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    heureVisite: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    proprietaire: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    adresseBien: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    ville: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    codePostal: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    typeBien: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    prix: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    informations_complementaires: {
      type: DataTypes.STRING,
      allowNull: true,
    },
  });

  Visite.associate = (models) => {
    Visite.belongsTo(models.Users, {
      foreignKey: "email",
      targetKey: "email",
      as: "user",
      onDelete: "CASCADE",
      allowNull: false,
    });

    Visite.hasMany(models.Chat, {
      foreignKey: "visiteId",
      as: "chats",
    });
  };
  return Visite;
};
