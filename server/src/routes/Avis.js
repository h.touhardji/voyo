const express = require("express");
const router = express.Router();
const { Avis, Users, Profile, ProfileVisiteur } = require("../models");
const validateToken = require("../middlewares/AuthMiddleware");

/**
 * @description Get all user's avis
 * @api /avis/user-avis
 * @access Private
 * @type GET
 */
router.get("/", validateToken, async (req, res) => {
	try {
		const receveurId = req.user.email; // Récupérer l'ID de l'utilisateur à partir du token
		const avis = await Avis.findAll({
			where: { receveurId },
			include: [
				{
					model: Users,
					as: "donneur",
					attributes: ["firstname", "lastname", "city", "userImg", "email"],
				},
			],
		});

		return res.status(200).json({ success: true, avis });
	} catch (err) {
		console.error(err);
		return res.status(500).json({
			success: false,
			message: "Impossible de récupérer les avis de l'utilisateur.",
		});
	}
});

/**
 * @description Create an avis
 * @api /avis
 * @access Private
 * @type POST
 */
router.post("/", validateToken, async (req, res) => {
	try {
		const { note, commentaire, emailProfilVisiteurNote } = req.body;
		const donneurId = req.user.email;

		// Vérifiez d'abord si un utilisateur avec l'e-mail de receveurId existe
		const receveur = await Users.findOne({
			where: { email: emailProfilVisiteurNote },
		});
		const receveurProfile = await Profile.findOne({
			where: { email: emailProfilVisiteurNote },
		});
		const receveurProfileVisiteur = await ProfileVisiteur.findOne({
			where: { email: emailProfilVisiteurNote },
		});
		if (!receveur || !receveurProfile) {
			return res.status(400).json({
				success: false,
				message: "L'utilisateur destinataire n'existe pas.",
			});
		}

		if (!receveurProfile.visiteur) {
			return res.status(403).json({
				success: false,
				message: "L'utilisateur n'est pas un visiteur.",
			});
		}

		const isAvisStillExist = await Avis.findOne({
			where: { donneurId: donneurId, receveurId: emailProfilVisiteurNote },
		});
		if (isAvisStillExist) {
			return res.status(200).json({ success: true, isAvisStillExist });
		} else {
			const receveurId = receveurProfile.email;
			const avis = await Avis.create({
				note,
				commentaire,
				donneurId,
				receveurId,
			});

			await receveurProfileVisiteur.update({
				nbAvis: receveurProfileVisiteur.nbAvis + 1,
			});

			return res.status(201).json({ success: true, avis });
		}
	} catch (error) {
		console.error(error);
		return res
			.status(500)
			.json({ success: false, message: "Impossible de créer l'avis." });
	}
});

/**
 * @description Update an avis
 * @api /avis/:avisId
 * @access Private
 * @type PUT
 */
router.put("/:avisId", validateToken, async (req, res) => {
	try {
		const avisId = req.params.avisId;
		const { note, commentaire } = req.body;
		const avis = await Avis.findByPk(avisId);

		if (!avis) {
			return res
				.status(404)
				.json({ success: false, message: "Avis introuvable." });
		}

		// Vérifiez si l'utilisateur est l'auteur de l'avis
		if (avis.donneurId !== req.user.email) {
			return res.status(403).json({
				success: false,
				message: "Vous n'êtes pas autorisé à modifier cet avis.",
			});
		}

		avis.note = note;
		avis.commentaire = commentaire;
		await avis.save();

		return res.status(200).json({ success: true, avis });
	} catch (error) {
		console.error(error);
		return res
			.status(500)
			.json({ success: false, message: "Impossible de mettre à jour l'avis." });
	}
});

/**
 * @description Delete an avis
 * @api /avis/:avisId
 * @access Private
 * @type DELETE
 */
router.delete("/:avisId", validateToken, async (req, res) => {
	try {
		const avisId = req.params.avisId;
		const avis = await Avis.findByPk(avisId);

		if (!avis) {
			return res
				.status(404)
				.json({ success: false, message: "Avis introuvable." });
		}

		// Vérifiez si l'utilisateur est l'auteur de l'avis
		if (avis.donneurId !== req.user.email) {
			return res.status(403).json({
				success: false,
				message: "Vous n'êtes pas autorisé à supprimer cet avis.",
			});
		}

		await avis.destroy();

		return res.status(204).end();
	} catch (error) {
		console.error(error);
		return res
			.status(500)
			.json({ success: false, message: "Impossible de supprimer l'avis." });
	}
});

module.exports = router;
