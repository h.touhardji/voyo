const express = require("express");
const router = express.Router();
const validateToken = require("../middlewares/AuthMiddleware");
const { Chat, Users } = require("../models");
const { Sequelize } = require("sequelize");

/**
 * @description Create a chat
 * @api /chat/api/
 * @access private
 * @type POST
 */
router.post("/", validateToken, async (req, res) => {
  try {
    // Extrait les données de la requête
    const { firstEmail, secondEmail } = req.body;

    // Recherchez les utilisateurs en utilisant leurs identifiants
    const firstUser = await Users.findOne({
      where: {
        email: firstEmail,
      },
    });
    const secondUser = await Users.findOne({
      where: {
        email: secondEmail,
      },
    });

    // Vérifiez si les utilisateurs existent
    if (!firstUser || !secondUser) {
      return res.status(400).json({
        success: false,
        message: "Utilisateurs introuvables.",
      });
    }

    // Vérifiez si un chat existe déjà entre ces utilisateurs

    const existingChat = await Chat.findOne({
      where: {
        user1Email: firstEmail,
        user2Email: secondEmail,
      },
    });

    if (existingChat) {
      return res.status(200).json({
        success: true,
        message: existingChat,
      });
    }

    // // Créez un chat en associant les utilisateurs

    const newChat = await Chat.create({
      status: 1,
    });

    await newChat.setUser1(firstUser);
    await newChat.setUser2(secondUser);
    return res.status(201).json({
      success: true,
      message: newChat,
    });
  } catch (err) {
    // Gestion des erreurs
    console.log(err);
    return res.status(400).json({
      success: false,
      message: "Impossible de créer le chat.",
    });
  }
});

/**
 * @description Get all chats for a user
 * @api /chat/api/
 * @access private
 * @type GET
 */
router.get("/", validateToken, async (req, res) => {
  try {
    // Extrait l'ID de l'utilisateur à partir du token
    const userEmail = req.user.email;
    // Recherchez tous les chats de l'utilisateur

    const userChats = await Chat.findAll({
      where: {
        [Sequelize.Op.or]: [
          {
            user1Email: userEmail,
          },
          {
            user2Email: userEmail,
          },
        ],
      },
      include: [
        {
          model: Users,
          as: "User1",
          attributes: ["firstname", "lastname", "city", "userImg", "email"],
        },
        {
          model: Users,
          as: "User2",
          attributes: ["firstname", "lastname", "city", "userImg", "email"],
        },
      ],
    });
    return res.status(200).json({
      success: true,
      message: userChats,
    });
  } catch (err) {
    console.log(err);
    return res.status(400).json({
      success: false,
      message: "Impossible de récupérer les chats de l'utilisateur.",
    });
  }
});

/**
 * @description Delete a chat
 * @api /chat/api/:chatId
 * @access private
 * @type DELETE
 */
router.delete("/:chatId", validateToken, async (req, res) => {
  try {
    // Extrait l'ID de l'utilisateur à partir du token
    const userId = req.user.email;

    // Extrait l'ID du chat à supprimer depuis les paramètres de la requête
    const chatId = req.params.chatId;

    // Recherchez le chat pour vous assurer que l'utilisateur a le droit de le supprimer
    const chat = await Chat.findOne({
      where: {
        id: chatId,
      },
    });

    if (!chat) {
      return res.status(404).json({
        success: false,
        message: "Chat introuvable.",
      });
    }

    // Vérifiez si l'utilisateur a le droit de supprimer ce chat
    if (chat.user1Email !== userId && chat.user2Email !== userId) {
      return res.status(403).json({
        success: false,
        message: "Vous n'avez pas le droit de supprimer ce chat.",
      });
    }

    // Supprimez le chat
    await chat.destroy();

    return res.status(200).json({
      success: true,
      message: "Chat supprimé avec succès.",
    });
  } catch (err) {
    console.log(err);
    return res.status(400).json({
      success: false,
      message: "Impossible de supprimer le chat.",
    });
  }
});

module.exports = router;
