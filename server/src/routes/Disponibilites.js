const { Router } = require("express");
const { Disponibilites, Users } = require("../models");
const SlugGenerator = require("../functions/slug-generator");
const validator = require("../middlewares/validator-middleware");
const { postValidations } = require("../validators/post-validators");
const validateToken = require("../middlewares/AuthMiddleware");

const router = Router();

function isDateSupperieurDateDuJour(dateString) {
  const [day, month, year] = dateString.split("/");
  const dateObj = new Date(`${year}-${month}-${day}`);
  const dateDuJour = new Date();
  return dateDuJour <= dateObj;
}

/**
 * @description To create a new disponibilite
 * @api POST /api/disponibilite
 * @access private
 */
router.post("", validateToken, async (req, res) => {
  try {
    const { body } = req;

    const user = await Users.findOne({
      where: {
        email: req.user.email,
      },
    });

    if (!user) {
      return res.json({
        code: 404,
        success: false,
        message: "L'utilisateur n'a pas été trouvé.",
      });
    }

    const newDisponibilite = await Disponibilites.create({
      dataDisponibilites: body.date,
      recurence: "NULL",
      heureDebut: "NULL",
      heureFin: "NULL",
      email: req.user.email,
    });

    const disponibilites = await Disponibilites.findAll({
      where: {
        email: req.user.email,
      },
    });

    const datesSet = new Set();

    disponibilites.filter(async (disponibilite) => {
      const dateDisponibilite = disponibilite.dataValues.dataDisponibilites;

      if (!datesSet.has(dateDisponibilite)) {
        datesSet.add(dateDisponibilite);
        return true;
      } else {
        await Disponibilites.destroy({
          where: {
            id: disponibilite.dataValues.id,
          },
        });
      }
      return false;
    });

    // Je dois m'assurer que les vielles dates doivent etre supprimers a chaque mise à jour des disponibilit
    disponibilites.forEach(async (dispo) => {
      const dateDisponibilites = dispo.dataValues.dataDisponibilites;
      if (!isDateSupperieurDateDuJour(dateDisponibilites)) {
        await Disponibilites.destroy({
          where: {
            id: dispo.dataValues.id,
          },
        });
        console.log(`Disponibilité supprimée avec succès: ${dispo}`);
      }
    });

    return res.status(201).json({
      disponibilite: newDisponibilite,
      success: true,
      message: "Disponibilite created successfully.",
    });
  } catch (err) {
    console.error(err);
    return res.status(400).json({
      success: false,
      message: "Unable to create the disponibilite.",
    });
  }
});

/**
 * @description Get disponibilites by ID
 * @api GET /api/disponibilite/:id
 * @access private
 */
router.get("", validateToken, async (req, res) => {
  try {
    const disponibilites = await Disponibilites.findAll({
      where: {
        email: req.user.email,
      },
    });
    if (!disponibilites) {
      return res.status(404).json({
        success: false,
        message: "disponibilites not found.",
      });
    }

    return res.status(200).json({
      disponibilites,
      success: true,
      message: "Get disponibilites by ID.",
    });
  } catch (err) {
    console.error(err);
    return res.status(500).json({
      success: false,
      message: "Unable to fetch the disponibilites.",
    });
  }
});

/**
 * @description To delete a disponibilite by ID
 * @api DELETE /api/disponibilite/:id
 * @access private
 */
router.delete("/:id", validateToken, async (req, res) => {
  try {
    const { id } = req.params;

    // Find disponibilite by ID
    const disponibilite = await Disponibilites.findOne({
      where: {
        id: id,
      },
    });

    if (!disponibilite) {
      return res.status(404).json({
        success: false,
        message: "Disponibilite not found.",
      });
    }

    await disponibilite.destroy();

    return res.status(200).json({
      success: true,
      message: "Disponibilite deleted successfully.",
    });
  } catch (err) {
    console.error(err);
    return res.status(400).json({
      success: false,
      message: "Unable to delete the disponibilite.",
    });
  }
});

module.exports = router;
