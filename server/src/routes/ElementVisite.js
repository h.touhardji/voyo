const { Router } = require("express");
const { ElementVisites } = require("../models");
const validateToken = require("../middlewares/AuthMiddleware");

const router = Router();

/**
 * @description To create a new element de visite
 * @api POST /api/element-visite
 * @access private
 */
router.post("", validateToken, async (req, res) => {
  try {
    const { body } = req;

    const isExistelementVisite = await ElementVisites.findAll({
      where: {
        nom: body.nom,
        visiteId: body.visiteId,
      },
    });

    if (!isExistelementVisite || isExistelementVisite.length > 0) {
      return res.status(200).json({
        elementVisite: isExistelementVisite,
        success: true,
        message: "Element de visite existe déjà.",
      });
    }
    const newElementVisite = await ElementVisites.create({
      nom: body.nom,
      commentaire: body.commentaire || null,
      visiteId: body.visiteId,
    });

    return res.status(201).json({
      elementVisite: newElementVisite,
      success: true,
      message: "Element de visite created successfully.",
    });
  } catch (err) {
    console.error(err);
    return res.status(400).json({
      success: false,
      message: "Unable to create the element de visite.",
    });
  }
});

/**
 * @description Get all elements de visite
 * @api GET /api/element-visites
 * @access private
 */
router.get("/", validateToken, async (req, res) => {
  try {
    const visiteID = req.query.visiteId;
    const elementVisites = await ElementVisites.findAll({
      where: {
        visiteId: visiteID,
      },
    });
    return res.status(200).json({
      elementVisites,
      success: true,
      message: "Get elements de visite by ID.",
    });
  } catch (err) {
    console.error(err);
    return res.status(500).json({
      success: false,
      message: "Unable to fetch the element de visite.",
    });
  }
});

/**
 * @description Get element de visite by ID
 * @api GET /api/element-visite/:id
 * @access private
 */
router.get("/:id", validateToken, async (req, res) => {
  try {
    const { id } = req.params;

    const elementVisite = await ElementVisites.findByPk(id);

    if (!elementVisite) {
      return res.status(404).json({
        success: false,
        message: "Element de visite not found.",
      });
    }

    return res.status(200).json({
      elementVisite,
      success: true,
      message: "Get element de visite by ID.",
    });
  } catch (err) {
    console.error(err);
    return res.status(500).json({
      success: false,
      message: "Unable to fetch the element de visite.",
    });
  }
});

/**
 * @description To update an element de visite by ID
 * @api PUT /api/element-visite/:id
 * @access private
 */
router.put("/:id", validateToken, async (req, res) => {
  try {
    const { id } = req.params;
    const { body } = req;
    const { visiteId } = req.query; // Récupérer visiteId depuis les paramètres de requête

    // Find element de visite by ID
    const elementVisite = await ElementVisites.findOne({
      where: {
        id: id,
      },
    });

    if (!elementVisite) {
      return res.status(404).json({
        success: false,
        message: "Element de visite not found.",
      });
    }
    // Update element de visite
    await elementVisite.update({
      nom: body.nom,
      commentaire: body.commentaire,
      visiteId: visiteId,
    });

    return res.status(200).json({
      elementVisite,
      success: true,
      message: "Element de visite updated successfully.",
    });
  } catch (err) {
    console.error(err);
    return res.status(400).json({
      success: false,
      message: "Unable to update the element de visite.",
    });
  }
});

/**
 * @description To delete an element de visite by ID
 * @api DELETE /api/element-visite/:id
 * @access private
 */
router.delete("/:id", validateToken, async (req, res) => {
  try {
    const { id } = req.params;

    // Find element de visite by ID
    const elementVisite = await ElementVisites.findByPk(id);

    if (!elementVisite) {
      return res.status(404).json({
        success: false,
        message: "Element de visite not found.",
      });
    }

    await elementVisite.destroy();

    return res.status(200).json({
      success: true,
      message: "Element de visite deleted successfully.",
    });
  } catch (err) {
    console.error(err);
    return res.status(400).json({
      success: false,
      message: "Unable to delete the element de visite.",
    });
  }
});

module.exports = router;
