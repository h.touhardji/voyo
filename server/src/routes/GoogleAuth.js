const express = require("express");
const router = express.Router();
const googleStrategy = require("passport-google-oauth20").Strategy;
const passport = require("passport");
const jwt = require("jsonwebtoken");
const db = require("../models");
const { Users, Profile } = require("../models");

const JWT_SECRET = process.env.JWT_SECRET;
const FRONT_URL = process.env.FRONT_URL;
const GOOGLE_CLIENT_ID = process.env.GOOGLE_CLIENT_ID;
const GOOGLE_CLIENT_SECRET = process.env.GOOGLE_CLIENT_SECRET;

passport.serializeUser((user, done) => {
  const serializedUser = {
    id: user.id,
    email: user.dataValues.email,
  };
  done(null, serializedUser);
});

passport.deserializeUser((user, done) => {
  // Retrieve user information from the session.
  done(null, user);
});

passport.use(
  new googleStrategy(
    {
      clientID: GOOGLE_CLIENT_ID,
      clientSecret: GOOGLE_CLIENT_SECRET,
      callbackURL: "https://voyo.fr/api/auth/google/callback",
      scope: ["profile", "email"],
    },
    (accessToken, refreshToken, profile, done) => {
      // Handle user profile data here, you can customize this part

      Users.findOne({ where: { email: profile._json.email } })
        .then(async (user) => {
          if (user) {
            return done(null, user);
          } else {
            const newUser = {
              firstname: profile._json.given_name,
              lastname: profile._json.family_name,
              googleId: profile.id,
              userImg: profile.photos[0].value,
              email: profile.emails[0].value,
              createdAt: new Date(),
              updatedAt: new Date(),
              password: "NULL",
              birthdate: null,
              city: "Null",
              phoneNumber: null,
              verified: true,
              verificationCode: null,
              resetPasswordToken: null,
              resetPasswordExpiresIn: null,
            };
            const createdUser = await Users.create(newUser);
            const createdProfile = await Profile.create({
              email: profile.emails[0].value,
              visiteur: false,
              userImg: profile.photos[0].value,
            });

            await createdProfile.save();
            await createdUser.save();

            return done(null, createdUser);
          }
        })
        .catch((err) => {
          return done(err, false);
        });
    }
  )
);

router.get(
  "/google",
  passport.authenticate("google", { scope: ["profile", "email"] })
);

router.get("/google/callback", passport.authenticate("google"), (req, res) => {
  if (req.user) {
    const user = req.user.dataValues;
    const accessToken = jwt.sign({ email: user.email }, JWT_SECRET, {
      expiresIn: 86400,
    });
    // Rediriger l'utilisateur vers la page d'atterrissage avec le token
    res.redirect(`${FRONT_URL}authRedirect?token=${accessToken}`);
  } else {
    res.status(401).json({ message: "Authentication failed" });
  }
});

module.exports = router;
