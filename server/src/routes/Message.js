const express = require("express");
const router = express.Router();
const { Message, Chat } = require("../models");
const validateToken = require("../middlewares/AuthMiddleware");

// Créer un message
router.post("/", validateToken, async (req, res) => {
	try {
		const { content, chatId } = req.body;
		const userEmail = req.user.email; // Récupérer l'ID de l'utilisateur à partir du token

		const chat = await Chat.findByPk(chatId);

		if (!chat) {
			return res
				.status(404)
				.json({ success: false, message: "Chat introuvable." });
		}

		const message = await Message.create({ content, chatId, userEmail });

		return res.status(201).json({ success: true, message });
	} catch (err) {
		console.error(err);
		return res
			.status(500)
			.json({ success: false, message: "Impossible de créer le message." });
	}
});

// Obtenir tous les messages d'un chat triés du plus récent au plus ancien
router.get("/:chatId", validateToken, async (req, res) => {
	try {
		const chatId = req.params.chatId;

		const chat = await Chat.findByPk(chatId);

		if (!chat) {
			return res
				.status(404)
				.json({ success: false, message: "Chat introuvable." });
		}

		const messages = await Message.findAll({
			where: { chatId },
			order: [["createdAt", "DESC"]], // Tri par date de création, du plus récent au plus ancien
		});

		return res.status(200).json({ success: true, messages });
	} catch (err) {
		console.error(err);
		return res.status(500).json({
			success: false,
			message: "Impossible de récupérer les messages.",
		});
	}
});

router.get("/:chatId/last-message", validateToken, async (req, res) => {
	try {
		const chatId = req.params.chatId;

		const chat = await Chat.findByPk(chatId);

		if (!chat) {
			return res
				.status(404)
				.json({ success: false, message: "Chat introuvable." });
		}

		const lastMessage = await Message.findOne({
			where: { chatId },
			order: [["createdAt", "DESC"]], // Tri par date de création, du plus récent au plus ancien
		});

		return res.status(200).json({ success: true, lastMessage });
	} catch (err) {
		console.error(err);
		return res.status(500).json({
			success: false,
			message: "Impossible de récupérer le dernier message du chat.",
		});
	}
});

// Mettre à jour un message
router.put("/:messageId", validateToken, async (req, res) => {
	try {
		const messageId = req.params.messageId;
		const { content } = req.body;

		const message = await Message.findByPk(messageId);

		if (!message) {
			return res
				.status(404)
				.json({ success: false, message: "Message introuvable." });
		}

		// Vérifier si l'utilisateur est l'auteur du message
		if (message.userEmail !== req.user.email) {
			return res.status(403).json({
				success: false,
				message: "Vous n'êtes pas autorisé à modifier ce message.",
			});
		}

		message.content = content;
		await message.save();

		return res.status(200).json({ success: true, message });
	} catch (err) {
		console.error(err);
		return res.status(500).json({
			success: false,
			message: "Impossible de mettre à jour le message.",
		});
	}
});

// Supprimer un message
router.delete("/:messageId", validateToken, async (req, res) => {
	try {
		const messageId = req.params.messageId;

		const message = await Message.findByPk(messageId);

		if (!message) {
			return res
				.status(404)
				.json({ success: false, message: "Message introuvable." });
		}

		// Vérifier si l'utilisateur est l'auteur du message
		if (message.userEmail !== req.user.email) {
			return res.status(403).json({
				success: false,
				message: "Vous n'êtes pas autorisé à supprimer ce message.",
			});
		}

		await message.destroy();

		return res.status(204).end();
	} catch (err) {
		console.error(err);
		return res
			.status(500)
			.json({ success: false, message: "Impossible de supprimer le message." });
	}
});

module.exports = router;
