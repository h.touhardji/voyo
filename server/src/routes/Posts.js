const { Router } = require("express");
const { Posts, User, Likes } = require("../models");
const SlugGenerator = require("../functions/slug-generator");
const validator = require("../middlewares/validator-middleware");
const { postValidations } = require("../validators/post-validators");
const { uploadPostImage: uploader } = require("../middlewares/uploader");
const validateToken = require("../middlewares/AuthMiddleware");

const router = Router();
const DOMAIN = process.env.APP_DOMAIN;

/**
 * @description To Upload Posts Image
 * @api /posts/api/post-image-upload
 * @access private
 * @type POST
 */
router.post(
  "/api/post-image-upload",
  validateToken,
  uploader.single("image"),
  async (req, res) => {
    try {
      let { file } = req;
      let filename = DOMAIN + "post-images/" + file.filename;
      return res.status(200).json({
        filename,
        success: true,
        message: "Image Uploaded Successfully.",
      });
    } catch (err) {
      return res.status(400).json({
        success: false,
        message: "Unable to create the post.",
      });
    }
  }
);

/**
 * @description To create a new post by the authenticated User
 * @api /posts/api/create-post
 * @access private
 * @type POST
 */
router.post(
  "/api/create-post",
  validateToken,
  postValidations,
  validator,
  async (req, res) => {
    try {
      // Create a new Posts
      let { body } = req;
      let post = new Posts({
        author: req.user.email,
        ...body,
        slug: SlugGenerator(body.title),
      });
      await post.save();
      return res.status(201).json({
        post,
        success: true,
        message: "Your post is published.",
      });
    } catch (err) {
      console.log(err);
      return res.status(400).json({
        success: false,
        message: "Unable to create the post.",
      });
    }
  }
);

/**
 * @description Get all posts by the authenticated User
 * @api /posts/api/all-posts
 * @access private
 * @type GET
 */
router.get("/api/all-posts", validateToken, validator, async (req, res) => {
  try {
    let posts = await Posts.findAll();
    if (!posts || posts.length === 0) {
      return res.status(404).json({
        success: false,
        message: "No posts found.",
      });
    }
    return res.status(200).json({
      posts,
      success: true,
      message: "Get all posts.",
    });
  } catch (err) {
    console.error(err);
    return res.status(500).json({
      success: false,
      message: "Unable to fetch posts.",
    });
  }
});

/**
 * @description Get post by the authenticated User
 * @api /posts/api/post/:id
 * @access private
 * @type GET
 */
router.get("/api/post/:postId", validateToken, validator, async (req, res) => {
  try {
    const { postId } = req.params;

    // Utilisez la méthode findOne pour obtenir un post par son ID
    const post = await Posts.findOne({
      where: { id: postId },
    });

    if (!post) {
      return res.status(404).json({
        success: false,
        message: "Post not found.",
      });
    }

    return res.status(200).json({
      post,
      success: true,
      message: "Get post by ID.",
    });
  } catch (err) {
    console.error(err);
    return res.status(500).json({
      success: false,
      message: "Unable to fetch the post.",
    });
  }
});

/**
 * @description To update a post by the authenticated User
 * @api /posts/api/update-post
 * @access private
 * @type PUT
 */
router.put(
  "/api/update-post/:id",
  validateToken,
  postValidations,
  validator,
  async (req, res) => {
    try {
      const { id } = req.params;
      const { user, body } = req;
      const post = await Posts.findOne({
        where: { id: id },
      });

      if (!post) {
        return res.status(404).json({
          success: false,
          message: "Post not found.",
        });
      }
      await Posts.update(
        {
          title: body.title,
          content: body.content,
          slug: SlugGenerator(body.title),
        },
        {
          where: { id: id },
        }
      );
      const updatedPost = await Posts.findOne({
        where: { id: id },
      });
      return res.status(200).json({
        post: updatedPost,
        success: true,
        message: "Post updated successfully.",
      });
    } catch (err) {
      console.error(err);
      return res.status(400).json({
        success: false,
        message: "Unable to update the post.",
      });
    }
  }
);

/**
 * @description To like a post by authenticated user
 * @api /posts/api/like-post
 * @access private
 * @type PUT
 */
router.post("/api/like-post/:id", validateToken, async (req, res) => {
  try {
    let { id } = req.params;

    // Recherchez le post avec l'ID spécifié dans la base de données MySQL
    const post = await Posts.findByPk(id);

    if (!post) {
      return res.status(404).json({
        success: false,
        message: "Post not found.",
      });
    }

    // Vérifiez si l'utilisateur a déjà aimé ce post en recherchant dans la table Likes
    const existingLike = await Likes.findByPostAndUser(id, req.user.email);

    if (existingLike) {
      return res.status(400).json({
        success: false,
        message: "You have already liked this post.",
      });
    }

    // Créez un enregistrement de Like dans la table Likes
    await Likes.create({
      postId: id,
      usereEmail: req.user.email,
    });

    return res.status(200).json({
      success: true,
      message: "You liked this post.",
    });
  } catch (err) {
    console.error(err);
    return res.status(500).json({
      success: false,
      message: "Unable to like the post. Please try again later.",
    });
  }
});

module.exports = router;
