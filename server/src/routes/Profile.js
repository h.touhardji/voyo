const { Router } = require("express");
const { Profile, Users } = require("../models");
const { upload } = require("../middlewares/uploader.js");
const validateToken = require("../middlewares/AuthMiddleware");
const fs = require("fs");
const { Sequelize } = require("sequelize");

const router = Router();
const DOMAIN = process.env.APP_DOMAIN;

/**
 * @description To Get the authenticated user's profile
 * @api /profiles/my-profile
 * @access Private
 * @type GET
 */

router.get("/my-profile", validateToken, async (req, res) => {
  try {
    const profile = await Profile.findOne({
      where: {
        email: req.user.email,
      },
    });
    if (!profile) {
      return res.status(404).json({
        success: false,
        message: "Your profile is not available.",
      });
    }
    return res.status(200).json({
      success: true,
      profile,
    });
  } catch (err) {
    console.log(err);
    return res.status(400).json({
      success: false,
      message: "Unable to get the profile.",
    });
  }
});

/**
 * @description To update autheticated user's profile
 * @type PUT <multipart-form> request
 * @api /profiles/update-profile
 * @access Private
 */
router.put(
  "/update-profile",
  validateToken,
  upload.single("userImg"),
  async (req, res) => {
    try {
      const { body, file, user } = req;
      const path = DOMAIN + "uploads/" + file.path.split("uploads/")[1];
      console.log("path : ", path);
      // Recherchez le profil de l'utilisateur par son adresse e-mail
      const userProfile = await Profile.findOne({
        where: { email: user.email },
      });
      const userAccount = await Users.findOne({ where: { email: user.email } });

      if (!userProfile || !userAccount) {
        return res.status(404).json({
          success: false,
          message: "Profile not found.",
        });
      }

      // Supprimez l'ancien fichier d'image de profil s'il existe
      if (
        userProfile.userImg &&
        userProfile.userImg.startsWith("https://voyo.fr/uploads")
      ) {
        const oldImagePath = userProfile.userImg.split(DOMAIN)[1];
        // Utilisez le système de fichiers pour supprimer le fichier
        const environment = process.env.NODE_ENV || "development";

        if (environment === "development") {
          fs.unlinkSync(`${__dirname}/../uploads/${oldImagePath}`);
        } else if (environment === "production") {
          fs.unlinkSync(`/var/www/html/voyo/${oldImagePath}`);
        }
      }
      // Mettez à jour le profil de l'utilisateur avec les nouvelles données
      const updatedProfile = await userProfile.update({
        userImg: path,
        score: body.score,
        description: body.description,
      });

      await userAccount.update({
        userImg: path,
      });

      return res.status(200).json({
        success: true,
        message: "Your profile has been updated successfully.",
        profile: updatedProfile,
      });
    } catch (err) {
      console.error(err);
      return res.status(400).json({
        success: false,
        message: "Unable to update the profile.",
      });
    }
  }
);

/**
 * @description To get user's profile with the username
 * @api /profiles/update-profile
 * @access Public
 * @type GET
 */
router.get("/profile-user/:username", async (req, res) => {
  try {
    let { username } = req.params;
    const userFind = await Users.findOne({
      where: {
        firstname: username,
      },
    });
    if (!userFind) {
      return res.status(404).json({
        success: false,
        message: "User not found.",
      });
    }
    const userProfile = await Profile.findOne({
      where: { email: userFind.email },
    });

    return res.status(200).json({
      profile: {
        userProfile,
        user: userFind.getUserInfo(),
      },
      success: true,
    });
  } catch (err) {
    console.log(err);
    return res.status(400).json({
      success: false,
      message: "Something went wrong.",
    });
  }
});

module.exports = router;
