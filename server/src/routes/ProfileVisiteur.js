const { Router } = require("express");
const { Profile, ProfileVisiteur, VilleVisiteur } = require("../models");
const validateToken = require("../middlewares/AuthMiddleware");
const { Sequelize } = require("sequelize");

const router = Router();

/**
 * @description To Get all visiteur
 * @api /profiles/visiteurs
 * @access Private
 * @type GET
 */

router.get("/visiteurs", validateToken, async (req, res) => {
  try {
    const visiteurList = await Profile.findAll({
      where: {
        email: {
          [Sequelize.Op.ne]: req.user.email,
        },
        visiteur: true,
      },
      include: {
        model: ProfileVisiteur,
        as: "profileVisiteur",
      },
    });
    return res.status(200).json({
      success: true,
      visiteurList,
    });
  } catch (err) {
    console.log(err);
    return res.status(400).json({
      success: false,
      message: "Erreur !",
    });
  }
});

/**
 * @description create profil visiteur
 * @api /profiles/visiteur
 * @access Private
 * @type POST
 */

router.post("/", validateToken, async (req, res) => {
  try {
    const {
      civilite,
      nom,
      prenom,
      dateDeNaissance,
      villeDeNaissance,
      adresse,
      codePostal,
      telephone,
      email,
      parlezDeVous,
      tarif,
      nbAvis,
      note,
      delaiAcceptation,
      preferenceModification,
      preferenceAnnulation,
      villesVisisteur,
    } = req.body;

    const emailUser = req.user.email;
    const userProfile = await Profile.findOne({
      where: {
        email: emailUser,
      },
    });

    if (!userProfile) {
      return res.json({
        code: 404,
        success: false,
        message: "Profil utilisateur introuvable",
      });
    }

    if (userProfile.visiteur) {
      return res.json({
        code: 400,
        success: false,
        message: "Le profil utilisateur est déjà un visiteur",
      });
    }

    const isExistProfileVisiteur = await ProfileVisiteur.findOne({
      where: {
        profileId: userProfile.id,
      },
    });

    if (isExistProfileVisiteur) {
      return res.json({
        code: 400,
        success: false,
        message: "Le profil visiteur existe déjà pour cet utilisateur",
      });
    }

    const profileVisiteur = await ProfileVisiteur.create({
      civilite,
      nom,
      prenom,
      date_de_naissance: dateDeNaissance,
      ville_de_naissance: villeDeNaissance,
      adresse_postale: adresse,
      code_postal: codePostal,
      telephone,
      email,
      parlez_de_vous: parlezDeVous,
      tarif,
      nbAvis,
      note,
      delai_acceptation: delaiAcceptation,
      preference_modification: preferenceModification,
      preference_annulation: preferenceAnnulation,
      profileId: userProfile.id,
      disponibleImmediatement: false,
    });

    if (profileVisiteur.id) {
      villesVisisteur.map(async (ville, index) => {
        try {
          const isExistVilleVisiteur = await VilleVisiteur.findAll({
            where: {
              visiteurId: profileVisiteur.id,
              ville: ville,
            },
          });
          if (isExistVilleVisiteur.length === 0) {
            await VilleVisiteur.create({
              visiteurId: profileVisiteur.id,
              ville: ville,
            });
          }
        } catch (err) {
          console.error(err);
          return res.status(400).json({
            success: false,
            message: "Impossible de créer la ville pour le visiteur.",
          });
        }
      });
    }

    await userProfile.update({
      visiteur: true,
    });

    return res.status(201).json({
      success: true,
      message: "Profil visiteur créé avec succès",
      data: profileVisiteur,
    });
  } catch (err) {
    console.log(err);
    return res.status(500).json({
      success: false,
      message: "Erreur lors de la création du profil visiteur",
    });
  }
});

/**
 * @description To Get a visiteur profile
 * @api /profiles/visiteur
 * @access Private
 * @type GET
 */
router.get("/", validateToken, async (req, res) => {
  try {
    const emailUser = req.user.email;
    const userProfile = await Profile.findOne({
      where: {
        email: emailUser,
        visiteur: true,
      },
      include: {
        model: ProfileVisiteur,
        as: "profileVisiteur",
      },
    });

    if (!userProfile) {
      return res.status(404).json({
        success: false,
        message: "Profil visiteur introuvable",
      });
    }

    return res.status(200).json({
      success: true,
      data: userProfile,
    });
  } catch (err) {
    console.log(err);
    return res.status(500).json({
      success: false,
      message: "Erreur lors de la récupération du profil visiteur",
    });
  }
});

/**
 * @description To Update a visiteur profile
 * @api /profiles/visiteur
 * @access Private
 * @type PUT
 */
router.put("/", validateToken, async (req, res) => {
  try {
    const emailUser = req.user.email;
    const userProfile = await Profile.findOne({
      where: {
        email: emailUser,
        visiteur: true,
      },
    });

    if (!userProfile) {
      return res.status(404).json({
        success: false,
        message: "Profil visiteur introuvable",
      });
    }

    const {
      civilite,
      nom,
      prenom,
      date_de_naissance,
      ville_de_naissance,
      adresse_postale,
      code_postal,
      telephone,
      email,
      parlezDeVous,
      disponibleImmediatement,
    } = req.body;
    console.log(req.body);

    const profileVisiteur = await ProfileVisiteur.findOne({
      where: {
        profileId: userProfile.id,
      },
    });

    if (!profileVisiteur) {
      return res.status(404).json({
        success: false,
        message: "Profil visiteur introuvable",
      });
    }

    await userProfile.update({ visiteur: true });
    await profileVisiteur.update({
      civilite,
      nom,
      prenom,
      date_de_naissance,
      ville_de_naissance,
      adresse_postale,
      code_postal,
      telephone,
      email,
      parlez_de_vous: parlezDeVous,
      disponibleImmediatement,
    });

    return res.status(200).json({
      success: true,
      message: "Profil visiteur mis à jour avec succès",
    });
  } catch (err) {
    console.log(err);
    return res.status(500).json({
      success: false,
      message: "Erreur lors de la mise à jour du profil visiteur",
    });
  }
});

/**
 * @description To Delete a visiteur profile
 * @api /profiles/visiteur
 * @access Private
 * @type DELETE
 */
router.delete("/", validateToken, async (req, res) => {
  try {
    const emailUser = req.user.email;
    const userProfile = await Profile.findOne({
      where: {
        email: emailUser,
        visiteur: true,
      },
    });

    if (!userProfile) {
      return res.status(404).json({
        success: false,
        message: "Profil visiteur introuvable",
      });
    }

    const profileVisiteur = await ProfileVisiteur.findOne({
      where: {
        profileId: userProfile.id,
      },
    });

    if (!profileVisiteur) {
      return res.status(404).json({
        success: false,
        message: "Profil visiteur introuvable",
      });
    }

    await userProfile.update({ visiteur: false });
    await profileVisiteur.destroy();

    return res.status(200).json({
      success: true,
      message: "Profil visiteur supprimé avec succès",
    });
  } catch (err) {
    console.log(err);
    return res.status(500).json({
      success: false,
      message: "Erreur lors de la suppression du profil visiteur",
    });
  }
});

module.exports = router;
