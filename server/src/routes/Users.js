const {
  ResetPassword,
  RegisterValidations,
  AuthenticateValidations,
} = require("../validators");
const { join } = require("path");
const { Users, Profile, ProfileVisiteur } = require("../models");
const { Router } = require("express");
const { randomBytes } = require("crypto");
const sendEmailMessage = require("../functions/email-sender");
const Validator = require("../middlewares/validator-middleware");
const validateToken = require("../middlewares/AuthMiddleware");
const { hash } = require("bcryptjs");
const mailBodyApresInscription = require("../mailling/apres-inscription");
const mailBodyConfirmationEmail = require("../mailling/confirmation-email");
const mailBodyConfirmationModifierMotDePasse = require("../mailling/confirmation-reset-mdp");
const mailBodyModifierMotDePasse = require("../mailling/reset-mdp");

const router = Router();
const DOMAIN = process.env.APP_DOMAIN;
/**
 * @description To create a new Users Account
 * @api /users/register
 * @access Public
 * @type POST
 */
router.post("/register", RegisterValidations, Validator, async (req, res) => {
  try {
    const { email } = req.body;

    // Check if the user exists with that email
    const findUser = await Users.findOne({
      where: { email: email },
    });

    if (findUser) {
      return res.status(400).json({
        success: false,
        message:
          "L'email est déjà enregistré. Auriez-vous oublié votre mot de passe ? Essayez de le réinitialiser.",
      });
    }

    const user = new Users({
      ...req.body,
      verificationCode: randomBytes(20).toString("hex"),
    });

    // Envoi du premier e-mail
    const htmlBodyApresInscription = mailBodyApresInscription();
    await sendEmailMessage(user.email, "Bienvenue", htmlBodyApresInscription);

    // Envoi du deuxième e-mail
    const link = `${DOMAIN}api/users/verify-now/${user.verificationCode}`;
    const htmlBodyConfirmationEmail = mailBodyConfirmationEmail(link);
    await sendEmailMessage(
      user.email,
      "Vérifier le compte",
      htmlBodyConfirmationEmail
    );

    // Sauvegarde de l'utilisateur dans la base de données
    await user.save();

    // Création et sauvegarde du profil associé à l'utilisateur
    const profile = await Profile.create({
      email: email,
      visiteur: false,
    });

    await profile.save();

    return res.status(201).json({
      success: true,
      message:
        "Hurray! your account is created please verify your email address.",
    });
  } catch (err) {
    console.error(err);
    return res.status(500).json({
      success: false,
      message: "An error occurred.",
    });
  }
});

/**
 * @description To verify a new user's account via email
 * @api /users/verify-now/:verificationCode
 * @access PUBLIC <Only Via email>
 * @type GET
 */
router.get("/verify-now/:verificationCode", async (req, res) => {
  try {
    let { verificationCode } = req.params;
    const user = await Users.findOne({
      where: {
        verificationCode: verificationCode,
      },
    });
    if (!user) {
      return res.status(401).json({
        success: false,
        message: "Unauthorized access. Invalid verification code.",
      });
    }
    user.verified = true;
    user.verificationCode = undefined;
    await user.save();
    return res.sendFile(
      join(__dirname, "../templates/verification-success.html")
    );
  } catch (err) {
    console.log("ERR", err.message);
    return res.sendFile(join(__dirname, "../templates/errors.html"));
  }
});

/**
 * @description To authenticate an user and get auth token
 * @api /users/authenticate
 * @access PUBLIC
 * @type POST
 */
router.post(
  "/authenticate",
  AuthenticateValidations,
  Validator,
  async (req, res) => {
    try {
      const { email, password } = req.body;
      const user = await Users.findOne({
        where: {
          email: email,
        },
      });

      if (!user) {
        return res.status(404).json({
          success: false,
          message: "Username not found.",
        });
      }
      if (!(await user.comparePassword(password))) {
        return res.status(401).json({
          success: false,
          message: "Incorrect password.",
        });
      }

      const token = await user.generateJWT();
      return res.status(200).json({
        success: true,
        user: user.getUserInfo(),
        token: `Bearer ${token}`,
        message: "Hurray! You are now logged in.",
      });
    } catch (err) {
      return res.status(500).json({
        success: false,
        message: "An error occurred.",
      });
    }
  }
);

/**
 * @description To get the authenticated user's profile
 * @api /users/authenticate
 * @access Private
 * @type GET
 */
router.get("/authenticate", validateToken, async (req, res) => {
  return res.status(200).json({
    user: req.user,
  });
});

/**
 * @description To Get the authenticated user's profile
 * @api /profiles/my-profile
 * @access Private
 * @type GET
 */

router.get("/user-info", validateToken, async (req, res) => {
  try {
    const user = await Users.findOne({
      where: {
        email: req.user.email,
      },
      attributes: {
        exclude: [
          "password",
          "resetPasswordExpiresIn",
          "resetPasswordToken",
          "verificationCode",
        ],
      },
    });

    const profil = await Profile.findOne({
      where: {
        email: req.user.email,
      },
    });

    if (!user) {
      return res.status(404).json({
        success: false,
        message: "Your user is not available.",
      });
    }

    return res.status(200).json({
      success: true,
      user,
      isVisiteur: profil.visiteur,
    });
  } catch (err) {
    console.error(err);
    return res.status(400).json({
      success: false,
      message: "Unable to get the user.",
    });
  }
});

/**
 * @description To initiate the password reset process
 * @api /users/reset-password
 * @access Public
 * @type POST
 */
router.put("/reset-password", ResetPassword, Validator, async (req, res) => {
  try {
    let { email } = req.body;
    const user = await Users.findOne({
      where: {
        email: email,
      },
    });
    if (!user) {
      return res.json({
        code: 404,
        success: false,
        message: "L'utilisateurs avec l'e-mail est introuvable.",
      });
    }
    user.generatePasswordReset();
    await user.save();
    // Sent the password reset Link in the email.
    const link = `${DOMAIN}api/users/reset-password-now/${user.resetPasswordToken}`;
    let htmlBodyModifierMotDePasse = mailBodyModifierMotDePasse(link);
    await sendEmailMessage(
      user.email,
      " Modification de votre mot de passe",
      htmlBodyModifierMotDePasse
    );
    return res.json({
      code: 200,
      success: true,
      message: "Password reset link is sent your email.",
    });
  } catch (err) {
    return res.status(500).json({
      success: false,
      message: "An error occurred.",
    });
  }
});

/**
 * @description To resender reset password page
 * @api /users/reset-password/:resetPasswordToken
 * @access Restricted via email
 * @type GET
 */
router.get("/reset-password-now/:resetPasswordToken", async (req, res) => {
  try {
    let { resetPasswordToken } = req.params;
    let user = await Users.findOne({
      resetPasswordToken,
      resetPasswordExpiresIn: { $gt: Date.now() },
    });
    if (!user) {
      return res.status(401).json({
        success: false,
        message: "Password reset token is invalid or has expired.",
      });
    }
    return res.sendFile(join(__dirname, "../templates/password-reset.html"));
  } catch (err) {
    return res.sendFile(join(__dirname, "../templates/errors.html"));
  }
});

/**
 * @description To reset the password
 * @api /users/reset-password-now
 * @access Restricted via email
 * @type POST
 */
router.post("/reset-password-now", async (req, res) => {
  try {
    let { resetPasswordToken, password } = req.body;
    let user = await Users.findOne({
      resetPasswordToken,
      resetPasswordExpiresIn: { $gt: Date.now() },
    });
    if (!user) {
      return res.status(401).json({
        success: false,
        message: "Password reset token is invalid or has expired.",
      });
    }
    user.password = await hash(password, 10);
    user.resetPasswordToken = undefined;
    user.resetPasswordExpiresIn = undefined;
    await user.save();
    // Send notification email about the password reset successfull process
    let htmlBodyConfirmationModifierMotDePasse =
      mailBodyConfirmationModifierMotDePasse();
    await sendEmailMessage(
      user.email,
      " Modification de votre mot de passe réussie",
      htmlBodyConfirmationModifierMotDePasse
    );
    return res.status(200).json({
      success: true,
      message:
        "Your password reset request is complete and your password is resetted successfully. Login into your account with your new password.",
    });
  } catch (err) {
    return res.status(500).json({
      sucess: false,
      message: "Something went wrong.",
    });
  }
});

/**
 * @description Mettez à jour les informations du compte de l'utilisateur
 * @api /users/update-account
 * @access Privé
 * @type PUT
 */
router.put("/update-account", validateToken, async (req, res) => {
  try {
    const { firstname, lastname, phoneNumber, birthdate, city } = req.body;
    // Obtenez l'utilisateur à mettre à jour
    const user = await Users.findOne({
      where: {
        email: req.user.email,
      },
    });

    if (!user) {
      return res.status(404).json({
        success: false,
        message: "L'utilisateur n'a pas été trouvé.",
      });
    }

    // Mettez à jour les informations de l'utilisateur
    user.firstname = firstname || user.firstname;
    user.lastname = lastname || user.lastname;
    user.phoneNumber = phoneNumber || user.phoneNumber;
    console.log(birthdate);
    user.birthdate = birthdate ? birthdate : user.birthdate;

    user.city = city ? city : user.city;

    await user.save();

    return res.status(200).json({
      success: true,
      message: "Les informations du compte ont été mises à jour avec succès.",
    });
  } catch (err) {
    console.error("Erreur lors de la mise à jour du compte :", err);
    return res.status(500).json({
      success: false,
      message: "Une erreur s'est produite lors de la mise à jour du compte.",
    });
  }
});

/**
 * @description Supprimer le compte de l'utilisateur
 * @api /users/delete-account
 * @access Privé
 * @type DELETE
 */
router.delete("/delete-account", validateToken, async (req, res) => {
  try {
    // Récupérez l'utilisateur à supprimer
    const user = await Users.findOne({
      where: {
        email: req.user.email,
      },
    });

    if (!user) {
      return res.status(404).json({
        success: false,
        message: "L'utilisateur n'a pas été trouvé.",
      });
    }

    // Supprimez le profil associé à l'utilisateur
    await Profile.destroy({
      where: {
        email: req.user.email,
      },
    });

    // Supprimez l'utilisateur
    await user.destroy();

    return res.status(200).json({
      success: true,
      message: "Le compte a été supprimé avec succès.",
    });
  } catch (err) {
    console.log(err);
    return res.status(500).json({
      success: false,
      message: "Une erreur s'est produite lors de la suppression du compte.",
    });
  }
});

/**
 * @description To logout the user
 * @api /users/logout
 * @access Public
 * @type GET
 */
router.get("/logout", (req, res) => {
  req.logout();
  res.clearCookie("session");
  res.clearCookie("session.sig");
  res.status(200).json({ message: "Logout successful" });
});

module.exports = router;
