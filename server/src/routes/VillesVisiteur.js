const { Router } = require("express");
const { VilleVisiteur } = require("../models");
const validateToken = require("../middlewares/AuthMiddleware");

const router = Router();

/**
 * @description Pour créer une nouvelle ville pour un visiteur
 * @api POST /api/ville-visiteur
 * @access private
 */
router.post("", validateToken, async (req, res) => {
  try {
    const { body } = req;

    const isExistVilleVisiteur = await VilleVisiteur.findAll({
      where: {
        visiteurId: body.visiteurId,
        ville: body.ville,
      },
    });

    if (!isExistVilleVisiteur || isExistVilleVisiteur.length > 0) {
      return res.status(200).json({
        villesVisiteur: isExistVilleVisiteur,
        success: true,
        message: "Cette ville pour le visiteur existe déjà.",
      });
    }

    const newVilleVisiteur = await VilleVisiteur.create({
      visiteurId: body.visiteurId,
      ville: body.ville,
    });

    return res.status(201).json({
      villeVisiteur: newVilleVisiteur,
      success: true,
      message: "Ville pour le visiteur créée avec succès.",
    });
  } catch (err) {
    console.error(err);
    return res.status(400).json({
      success: false,
      message: "Impossible de créer la ville pour le visiteur.",
    });
  }
});

/**
 * @description Récupérer toutes les villes d'un visiteur
 * @api GET /api/ville-visiteur/:visiteurId
 * @access private
 */
router.get("/:visiteurId", validateToken, async (req, res) => {
  try {
    const { visiteurId } = req.params;

    const villesVisiteur = await VilleVisiteur.findAll({
      where: {
        visiteurId: visiteurId,
      },
    });

    if (!villesVisiteur || villesVisiteur.length === 0) {
      return res.status(404).json({
        success: false,
        message: "Aucune ville trouvée pour ce visiteur.",
      });
    }

    return res.status(200).json({
      villesVisiteur,
      success: true,
      message: "Villes du visiteur récupérées avec succès.",
    });
  } catch (err) {
    console.error(err);
    return res.status(500).json({
      success: false,
      message: "Impossible de récupérer les villes du visiteur.",
    });
  }
});

/**
 * @description Pour supprimer une ville d'un visiteur
 * @api DELETE /api/ville-visiteur/:id
 * @access private
 */
router.delete("/:id", validateToken, async (req, res) => {
  try {
    const { id } = req.params;

    const villeVisiteur = await VilleVisiteur.findByPk(id);

    if (!villeVisiteur) {
      return res.status(404).json({
        success: false,
        message: "Ville du visiteur non trouvée.",
      });
    }

    await villeVisiteur.destroy();

    return res.status(200).json({
      success: true,
      message: "Ville du visiteur supprimée avec succès.",
    });
  } catch (err) {
    console.error(err);
    return res.status(400).json({
      success: false,
      message: "Impossible de supprimer la ville du visiteur.",
    });
  }
});

module.exports = router;
