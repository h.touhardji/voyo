const express = require("express");
const router = express.Router();
const { Visites, Users, ProfileVisiteur, Chat, Message } = require("../models");
const validateToken = require("../middlewares/AuthMiddleware");
const { Sequelize } = require("sequelize");

/**
 * @description Create a visite
 * @api /visites
 * @access Private
 * @type POST
 */
router.post("/", validateToken, async (req, res) => {
  try {
    const {
      date,
      heureVisite,
      proprietaire,
      adresseBien,
      ville,
      codePostal,
      typeBien,
      prix,
      informationsComplementaires,
      visiteurEmail,
    } = req.body;

    const email = visiteurEmail;

    const visiteur = await ProfileVisiteur.findOne({
      where: { email: visiteurEmail },
    });

    if (!visiteur) {
      return res.status(400).json({
        success: false,
        message: "Le visiteur n'existe pas.",
      });
    }

    const visite = await Visites.create({
      date,
      heureVisite,
      proprietaire,
      adresseBien,
      ville,
      codePostal,
      typeBien,
      prix,
      informationsComplementaires,
      email,
    });

    // Extrait les données de la requête
    const secondEmail = req.user.email;
    const firstEmail = visiteurEmail;
    // Recherchez les utilisateurs en utilisant leurs identifiants
    const firstUser = await Users.findOne({
      where: {
        email: firstEmail,
      },
    });
    const secondUser = await Users.findOne({
      where: {
        email: secondEmail,
      },
    });

    // Vérifiez si les utilisateurs existent
    if (!firstUser || !secondUser) {
      return res.status(400).json({
        success: false,
        message: "Utilisateurs introuvables.",
      });
    }

    // Vérifiez si un chat existe déjà entre ces utilisateurs

    const existingChat = await Chat.findOne({
      where: {
        [Sequelize.Op.or]: [
          {
            [Sequelize.Op.and]: [
              { user1Email: firstEmail },
              { user2Email: secondEmail },
            ],
          },
          {
            [Sequelize.Op.and]: [
              { user1Email: secondEmail },
              { user2Email: firstEmail },
            ],
          },
        ],
      },
    });

    if (existingChat) {
      return res.status(403).json({
        success: false,
        message:
          "Impossible de créer un nouveau chat entre ces deux utilisateur.",
      });
    }

    const newChat = await Chat.create({
      status: 1,
      visiteId: visite.id,
    });

    await newChat.setUser1(firstUser);
    await newChat.setUser2(secondUser);
    if (
      informationsComplementaires &&
      informationsComplementaires.length > 0 &&
      informationsComplementaires !== ""
    ) {
      try {
        const chatId = newChat.id;
        const chat = await Chat.findByPk(chatId);

        if (!chat) {
          return res
            .status(404)
            .json({ success: false, message: "Chat introuvable." });
        }

        const userEmail = req.user.email;
        const content = `Ceci est un message automatique. Voici les informations complémentaires que la personne vous demande de regarder : ${informationsComplementaires}`;

        await Message.create({ content, chatId, userEmail });
      } catch (error) {
        console.error("Erreur lors de la création du message :", error);
        return res.status(500).json({
          success: false,
          message: "Erreur serveur lors de la création du message.",
        });
      }
    } else {
      return res.status(400).json({
        success: false,
        message: "Aucune information complémentaire fournie.",
      });
    }

    return res.status(201).json({ success: true, visite: visite });
  } catch (err) {
    console.error(err);
    return res.status(500).json({
      success: false,
      message: "Impossible de créer la visite.",
    });
  }
});

/**
 * @description Get all user's visites programmation
 * @api /visites/user-visites
 * @access Private
 * @type GET
 */
router.get("/user-visites", validateToken, async (req, res) => {
  try {
    const email = req.user.email; // Récupérer l'ID de l'utilisateur à partir du token

    const visites = await Visites.findAll({
      where: { email },
      include: {
        model: Users,
        as: "user", // Spécifiez l'alias ici
      },
    });

    return res.status(200).json({ success: true, visites });
  } catch (err) {
    console.error(err);
    return res.status(500).json({
      success: false,
      message: "Impossible de récupérer les visites de l'utilisateur.",
    });
  }
});

/**
 * @description Get ont user's visite programmation
 * @api /visites/:id
 * @access Private
 * @type GET
 */
router.get("/:visiteId", validateToken, async (req, res) => {
  try {
    const visiteId = req.params.visiteId;

    const visite = await Visites.findByPk(visiteId);

    if (!visite) {
      return res
        .status(404)
        .json({ success: false, message: "Visite introuvable." });
    }

    // Vérifier si l'utilisateur est le créateur de la visite
    if (visite.email !== req.user.email) {
      return res.status(403).json({
        success: false,
        message: "Vous n'êtes pas autorisé à modifier cette visite.",
      });
    }

    return res.status(200).json({ success: true, visite });
  } catch (err) {
    console.error(err);
    return res.status(500).json({
      success: false,
      message: "Impossible de mettre à jour la visite.",
    });
  }
});

/**
 * @description Update user's visite programmation
 * @api /visites/:id
 * @access Private
 * @type PUT
 */
router.put("/:visiteId", validateToken, async (req, res) => {
  try {
    const visiteId = req.params.visiteId;
    const {
      date,
      heureVisite,
      proprietaire,
      adresseBien,
      ville,
      codePostal,
      typeBien,
    } = req.body;

    const visite = await Visites.findByPk(visiteId);

    if (!visite) {
      return res
        .status(404)
        .json({ success: false, message: "Visite introuvable." });
    }

    // Vérifier si l'utilisateur est le créateur de la visite
    if (visite.email !== req.user.email) {
      return res.status(403).json({
        success: false,
        message: "Vous n'êtes pas autorisé à modifier cette visite.",
      });
    }

    visite.date = date;
    visite.heureVisite = heureVisite;
    visite.proprietaire = proprietaire;
    visite.adresseBien = adresseBien;
    visite.ville = ville;
    visite.codePostal = codePostal;
    visite.typeBien = typeBien;

    await visite.save();

    return res.status(200).json({ success: true, visite });
  } catch (err) {
    console.error(err);
    return res.status(500).json({
      success: false,
      message: "Impossible de mettre à jour la visite.",
    });
  }
});

/**
 * @description Remove user's visite programmation
 * @api /visites/:id
 * @access Private
 * @type DALETE
 */
router.delete("/:visiteId", validateToken, async (req, res) => {
  try {
    const visiteId = req.params.visiteId;

    const visite = await Visites.findByPk(visiteId);

    if (!visite) {
      return res
        .status(404)
        .json({ success: false, message: "Visite introuvable." });
    }

    // Vérifier si l'utilisateur est le créateur de la visite
    if (visite.email !== req.user.email) {
      return res.status(403).json({
        success: false,
        message: "Vous n'êtes pas autorisé à supprimer cette visite.",
      });
    }

    await visite.destroy();

    return res.status(204).end();
  } catch (err) {
    console.error(err);
    return res.status(500).json({
      success: false,
      message: "Impossible de supprimer la visite.",
    });
  }
});

module.exports = router;
