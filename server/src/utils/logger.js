const winston = require('winston');

const logger = winston.createLogger({
  level: 'info', // Niveau de log souhaité
  format: winston.format.json(), // Format des logs
  transports: [
    new winston.transports.Console(), // Affichage des logs dans la console
    new winston.transports.File({ filename: 'logs.log' }) // Enregistrement des logs dans un fichier
  ]
});

module.exports = logger;
