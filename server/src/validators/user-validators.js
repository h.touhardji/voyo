const { check } = require("express-validator")

const firstname = check("firstname", "Firstname is required.").not().isEmpty();
const lastname = check("lastname", "Lastname is required.").not().isEmpty();
const email = check("email", "Please provide a valid email address").isEmail();
const birthdate = check("birthdate", "Birthdate is required.").not().isEmpty();
const city = check("city", "City is required.").not().isEmpty();

const password = check(
  "password",
  "Password is required of minimum length of 6."
).isLength({
  min: 6,
});

const RegisterValidations = [password, firstname, lastname, email, birthdate, city];
const AuthenticateValidations = [email, password];
const ResetPassword = [email];

module.exports = {
  RegisterValidations,
  AuthenticateValidations,
  ResetPassword,
};